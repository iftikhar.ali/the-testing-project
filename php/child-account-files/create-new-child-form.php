<?php
if (!defined("SATLOC")) {
	$SATLOC = $_POST['SATLOC'];
	define("SATLOC",$SATLOC);
	}

$location = $_SERVER['DOCUMENT_ROOT']. SATLOC;
include ($location . '/wp-config.php');
include ($location . '/wp-load.php');
include ($location . '/wp-includes/pluggable.php');
global $wpdb;

$user_email 	= $_POST['useremail'];
$parent_id 		= $_POST['userid'];

get_header(); ?>

<div id="new-user-form" style="margin:25px;">
<h2>Create a New Child Account</h2>
<form action="<?php echo bloginfo('stylesheet_directory'); ?>/custom-functions/child/create-new-child.php" method="post">
	<p>Email: <?php echo $user_email; ?></p>

	<input type="hidden" value="<?php echo $user_email; ?>" name="user_email" />
	<input type="hidden" value="<?php echo $parent_id ?>" name="parent_id" />

	<label id="user_name" name="user_name">Child's User Name: </label><br />
	<input type="text" name="user_name" /><br />

	<label id="first_name" name="first_name">Child's First Name: </label><br />
	<input type="text" name="first_name" /><br />

	<label id="last_name" name="last_name">Child's Last Name: </label><br />
	<input type="text" name="last_name" /><br />

	<label id="zip" name="zip">Child's Zip Code: </label><br />
	<input type="text" name="zip" /><br />

	<label id="year" name="year">Child's Year of Birth: </label><br />
	<select name="year" />
        <option value="2000">2013</option>
        <option value="2000">2012</option>
        <option value="2000">2011</option>
        <option value="2000">2010</option>
        <option value="2000">2009</option>
        <option value="1999">2008</option>
        <option value="1998">2007</option>
        <option value="1997">2006</option>
        <option value="1996">2005</option>
        <option value="1995">2004</option>
        <option value="1994">2003</option>
        <option value="1993">2002</option>
        <option value="1992">2001</option>
        <option value="2000">2000</option>
        <option value="1999">1999</option>
        <option value="1998">1998</option>
        <option value="1997">1997</option>
        <option value="1996">1996</option>
        <option value="1995">1995</option>
        <option value="1994">1994</option>
        <option value="1993">1993</option>
        <option value="1992">1992</option>
        <option value="1991">1991</option>
        <option value="1990">1990</option>
        <option value="1989">1989</option>
        <option value="1988">1988</option>
        <option value="1987">1987</option>
        <option value="1986">1986</option>
        <option value="1985">1985</option>
        <option value="1984">1984</option>
        <option value="1983">1983</option>
        <option value="1982">1982</option>
        <option value="1981">1981</option>
        <option value="1980">1980</option>
        <option value="1979">1979</option>
        <option value="1978">1978</option>
        <option value="1977">1977</option>
        <option value="1976">1976</option>
        <option value="1975">1975</option>
        <option value="1974">1974</option>
        <option value="1973">1973</option>
        <option value="1972">1972</option>
        <option value="1971">1971</option>
        <option value="1970">1970</option>
        <option value="1969">1969</option>
        <option value="1968">1968</option>
        <option value="1967">1967</option>
        <option value="1966">1966</option>
        <option value="1965">1965</option>
        <option value="1964">1964</option>
        <option value="1963">1963</option>
        <option value="1962">1962</option>
        <option value="1961">1961</option>
        <option value="1960">1960</option>
        <option value="1959">1959</option>
        <option value="1958">1958</option>
        <option value="1957">1957</option>
        <option value="1956">1956</option>
        <option value="1955">1955</option>
	</select>
	<br />
  
	<label id="month" name="month">Child's Month of Birth: </label><br />
	<select name="month" />
    	<option value="January">January</option>
        <option value="February">February</option>
        <option value="March">March</option>
        <option value="April">April</option>
        <option value="May">May</option>
        <option value="June">June</option>
        <option value="July">July</option>
        <option value="August">August</option>
        <option value="September">September</option>
        <option value="October">October</option>
        <option value="November">November</option>
        <option value="December">December</option>
	</select>
	<br />

	<label id="pass" name="pass">Child's Password: </label><br />
	<input type="password" name="pass" /><br />
	<input type="hidden" value="<?php echo SATLOC; ?>" name="SATLOC" />
	<input type="submit" value="Create Child Account" />	
</form>	
</div>

<?php get_footer(); ?>