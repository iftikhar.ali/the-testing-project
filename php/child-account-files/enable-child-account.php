<?php
if (!defined("SATLOC")) {
	$SATLOC = $_POST['SATLOC'];
	define("SATLOC",$SATLOC);
	}

$location = $_SERVER['DOCUMENT_ROOT']. SATLOC;
include ($location . '/wp-config.php');
include ($location . '/wp-load.php');
include ($location . '/wp-includes/pluggable.php');
global $wpdb;
global $bp;

$meta_value	=	$_POST['active'];
$user_id	=	$_POST['user_id'];
$meta_key	=	'st_active_child';
$meta_key_lock		=	'wp_ul_disabled';

/*
echo '<p>Active: ' . $active . '</p>';
echo '<p>UserID: ' . $user_id . '</p>';
*/

update_user_meta( $user_id, $meta_key, $meta_value);
delete_user_meta( $user_id, $meta_key_lock);

//$check = get_userdata( $user_id );
//echo $check->st_active_child;

header('Location: ' . $bp->loggedin_user->domain . '/child-accounts/')
?>