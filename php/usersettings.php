<?php

class JSON_API_userSettings_Controller {

public function userSettings_api() {
global $json_api;
global $wpdb;
global $bp;
$userid 	= $_GET['userid'];
$r=0;

// Zip Code		= 3
// Parent  		= 4
// Country		= 42
// Birth Month 	= 46
// Birth Year	= 59

if(!$userid){
	$status = "error";
	$message = "We cannot find a User ID in your request. Check your formatting and try again.";
	$errorcode = 1;
	}
else {
	$status = "ok";
	$message = "User Found.";
	$errorcode = 0;
	}		
		
$zip = $wpdb->get_results("SELECT value 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 3");

$parent = $wpdb->get_results("SELECT value 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 4");	

$country = $wpdb->get_results("SELECT value 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 42");

$month = $wpdb->get_results("SELECT value 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 46");

$year = $wpdb->get_results("SELECT value 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 59");

$settings_obj = array(
	"zip"	=> $settings[4]
);

return array(
			"status" 		=> $status,
			"message"		=> $message,
			"errocode"		=> $errorcode,			
			"zip" 			=>$zip[0]->value,
			"parent"		=>$parent[0]->value,
			"country"		=>$country[0]->value,
			"month"			=>$month[0]->value,
			"year"			=>$year[0]->value,
			"debug"			=> "debugging"
	    );			
		
  }// end function

/*****************************/
/******** UPDATE USER ********/
/*****************************/

public function userSettings_update_api() {
global $json_api;
global $wpdb;
global $bp;
$userid 	= $_GET['userid'];
$zip		= $_GET['zip'];
$parent		= $_GET['parent'];
$country	= $_GET['country'];
$month		= $_GET['month'];
$year		= $_GET['year'];


// POSTAL CODE UPDATE
$zip_check = $wpdb->get_results("SELECT * 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 3");
$zipcode = $zip_check[0]->value;
$zip_id	= $zip_check[0]->id;

if($zipcode == NULL){
	$zip_update = $wpdb->get_results("
		INSERT INTO  `wp_bp_xprofile_data` (
			`id`,
			`field_id` ,
			`user_id` ,
			`value` ,
			`last_updated`
		)
		VALUES (
			NULL ,  '3',  '$userid',  '$zip',  ''
		);
	");
}// end if
elseif($zipcode > 0) {
	$zip_update = $wpdb->get_results("
	UPDATE  `wp_bp_xprofile_data` SET `value` =  '$zip' WHERE  `wp_bp_xprofile_data`.`id` = $zip_id LIMIT 1;	
	");
	$r =  $r + intval($wpdb->rows_affected);
}// end elseif

// PARENT UPDATE
$parent_check = $wpdb->get_results("SELECT * 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 4");
$parent_result = $parent_check[0]->value;
$parent_id	= $parent_check[0]->id;

if($parent_result == NULL){
	$parent_update = $wpdb->get_results("
		INSERT INTO  `wp_bp_xprofile_data` (
			`id`,
			`field_id` ,
			`user_id` ,
			`value` ,
			`last_updated`
		)
		VALUES (
			NULL ,  '4',  '$userid',  '$parent',  ''
		);
	");
}// end if
elseif($parent != NULL) {
	$parent_update = $wpdb->get_results("
	UPDATE  `wp_bp_xprofile_data` SET `value` =  '$parent' WHERE  `wp_bp_xprofile_data`.`id` = $parent_id LIMIT 1;
	");
	$r =  $r + intval($wpdb->rows_affected);
}// end elseif

// COUNTRY UPDATE
$country_check = $wpdb->get_results("SELECT * 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 42");
$country_result = $country_check[0]->value;
$country_id	= $country_check[0]->id;

if($country_result == NULL){
	$country_update = $wpdb->get_results("
		INSERT INTO  `wp_bp_xprofile_data` (
			`id`,
			`field_id` ,
			`user_id` ,
			`value` ,
			`last_updated`
		)
		VALUES (
			NULL ,  '42',  '$userid',  '$country',  ''
		);
	");
}// end if
elseif($country_check != NULL) {
	$country_update = $wpdb->get_results("
	UPDATE  `wp_bp_xprofile_data` SET `value` =  '$country' WHERE  `wp_bp_xprofile_data`.`id` = $country_id LIMIT 1;
	");
	$r =  $r + intval($wpdb->rows_affected);
}// end elseif

// YEAR UPDATE
$year_check = $wpdb->get_results("SELECT * 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 59");
$year_result = $year_check[0]->value;
$year_id	= $year_check[0]->id;

if($year_check == NULL){
	$year_update = $wpdb->get_results("
		INSERT INTO  `wp_bp_xprofile_data` (
			`id`,
			`field_id` ,
			`user_id` ,
			`value` ,
			`last_updated`
		)
		VALUES (
			NULL ,  '59',  '$userid',  '$year',  ''
		);
	");
}// end if
elseif($year_check != NULL) {
	$year_update = $wpdb->get_results("
	UPDATE  `wp_bp_xprofile_data` SET `value` =  '$year' WHERE  `wp_bp_xprofile_data`.`id` = $year_id LIMIT 1;
	");
	$r =  $r + intval($wpdb->rows_affected);
}// end elseif

// MONTH UPDATE
$month_check = $wpdb->get_results("SELECT * 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 46");
$month_result = $month_check[0]->value;
$month_id	= $month_check[0]->id;

if($month_check == NULL){
	$month_update = $wpdb->get_results("
		INSERT INTO  `wp_bp_xprofile_data` (
			`id`,
			`field_id` ,
			`user_id` ,
			`value` ,
			`last_updated`
		)
		VALUES (
			NULL ,  '46',  '$userid',  '$month',  ''
		);
	");
}// end if
elseif($month_check != NULL) {
	$month_update = $wpdb->get_results("
	UPDATE  `wp_bp_xprofile_data` SET `value` =  '$month' WHERE  `wp_bp_xprofile_data`.`id` = $month_id LIMIT 1;
	");
	$r =  $r + intval($wpdb->rows_affected);
}// end elseif

// Zip Code		= 3
// Parent  		= 4
// Country		= 42
// Birth Month 	= 46
// Birth Year	= 59

if(!$userid){
	$status = "error";
	$message = "We cannot find a User ID in your request. Check your formatting and try again.";
	$errorcode = 1;
	}
else {
	$status = "ok";
	$message = '<span style="color:red;">Settings Updated!</span>';
	$errorcode = 0;
	}		
		
return array(
			"status" 		=> $status,
			"message"		=> $message,
			"errocode"		=> $errorcode,			
			"zip" 			=> $zip,
			"parent"		=> $parent,
			"country"		=> $country,
			"month"			=> $month,
			"year"			=> $year,
			"query"			=> $wpdb->last_query,
			"rows"			=> $wpdb->rows_affected
	    );			
		
  }// end function
  
/******************************/
/******** GET MESSAGES ********/
/******************************/

public function userMessages_api() {
global $json_api;
global $wpdb;
global $bp;
$userid 	= $_GET['userid'];


if ( bp_has_message_threads('user_id='.$userid.'&box=inbox') ) : 

$m=0;
$n=0;
$messages = array();
while ( bp_message_threads() ) : 

$messages[$m]->$n = '<span>'.bp_message_thread().'</span>'; $n++;

$messages[$m]->$n .= '<li>'; $n++;
$messages[$m]->$n .= '<span>'.bp_message_thread_id() . '</span> | '; $n++;
$messages[$m]->$n .= '<span>'.bp_message_thread_has_unread() . '</span> | '; $n++;
$messages[$m]->$n .= '<span>'.bp_message_thread_from() . '</span> | '; $n++;
$messages[$m]->$n .= '<span>'.bp_message_thread_last_post_date() . '</span> | '; $n++;
$messages[$m]->$n .= '<span>'.bp_message_thread_subject() . '</span> | '; $n++;
$messages[$m]->$n .= '<span>'.bp_message_thread_excerpt() . '</span> | '; $n++;
$messages[$m]->$n .= '<a href="' . bp_message_thread_view_link() . '">Read</a>' . ' | '; $n++;
$messages[$m]->$n .= '<a href="' . bp_message_thread_delete_link() .'">Delete</a>' . ' | '; $n++;
$messages[$m]->$n .= '</li>'; 
$m++;
$n=0;
endwhile; 

?>



<?php else: 
$m=0;
$messages = array();


$messages[$m] .='  <div id="message" class="info">';
$messages[$m] .='  <p>There are no messages to display.</p>';
$messages[$m] .= ' </div>';
 
endif; 

$read=array();
$r=0; 
if ( bp_has_message_threads('user_id='.$userid.'&box=sentbox') ) : ?>

  <?php while ( bp_message_threads() ) : 

$read[$r] = bp_message_thread(); 
 
$read[$r] .= '<li>';
$read[$r] .= bp_message_thread_id(); 
$read[$r] .= bp_message_thread_has_unread();
$read[$r] .= bp_message_thread_from(); 
$read[$r] .= bp_message_thread_last_post_date();
$read[$r] .= bp_message_thread_subject(); 
$read[$r] .= bp_message_thread_excerpt(); 
$read[$r] .= '<a href="' . bp_message_thread_view_link() . '">Read</a>'; 
$read[$r] .= '<a href="' . bp_message_thread_delete_link() .'">Delete</a>';
$read[$r] .= '</li>'; 
$r++;
 endwhile; ?>
 
 
<?php else: 
 
$read[$r] .= '  <div id="message" class="info">';
$read[$r] .= '    <p>There are no messages to display.</p>';
$read[$r] .= '  </div>';
 
endif; 

return array(
			"status" 		=> 'ok',
			"messages"		=> $messages,
			"read"			=> $read
	    );			
		
  }// end function
  
/******************************/
/****** REPLY TO MESSAGE ******/
/******************************/
// Send the reply
function replyToMessage(){
        global $json_api;
        global $wpdb;
        global $bp;

        $sub                 = mysql_escape_string($_GET['sub']);
        $mes                 = mysql_escape_string($_GET['mes']);
        $tid                = mysql_escape_string($_GET['tid']);

$args = array( 'thread_id' => $tid, 'subject' => $sub, 'content' => $mes );

if (messages_new_message( $args ) )
{
return array(
        "status"         => "ok",
        "message"         => "Reply Sent!",
        "sub"                 =>        $sub,
        "mes"                 =>        $mes
);
}// end if we got all the parts
else {return array(
        "status"         => "error",
        "message"         => "Oops! Something happened and that reply was not sent.",
        "debug"                => $wpdb->last_error,
        "query"                => $wpdb->last_query
);
} // end if/else
                

}// end reply

/******************************/
/******* DELETE MESSAGE *******/
/******************************/
// See bp-messages-classes.php
function deleteMessage(){
global $wpdb;
global $bp;
$id         = mysql_escape_string($_GET['messageID']);
$UserID = mysql_escape_string($_GET['UserID']);
$tid         = mysql_escape_string($_GET['tid']);

$deleteItNow = $wpdb->prepare( "UPDATE {$bp->messages->table_name_recipients} SET is_deleted = 1 WHERE thread_id = $tid AND user_id = $UserID" );

if ( !$wpdb->query( $deleteItNow ) ) {
return array(
        "status" => "error",
        "message" => "Could not find that message...",
        "debug" => $wpdb->last_error,
        "query" => $wpdb->last_query,
);
} else {
$recipients = array();
$results    = $wpdb->get_results( $wpdb->prepare( "
        SELECT * FROM {$bp->messages->table_name_recipients} WHERE thread_id = $tid
        ") );
        
foreach ( (array) $results as $recipient ) {
        $recipients[$recipient->user_id] = $recipient;        
        $recipients = $wpdb->get_results( $wpdb->prepare( "
         SELECT id FROM {$bp->messages->table_name_recipients} WHERE thread_id = $tid AND is_deleted = 0
         " ) );
}// end foreach

if ( empty( $recipients ) ) {
// Delete all the messages
$wpdb->query( $wpdb->prepare( "DELETE FROM {$bp->messages->table_name_messages} WHERE thread_id = $tid" ) );
// Delete all the recipients
$wpdb->query( $wpdb->prepare( "DELETE FROM {$bp->messages->table_name_recipients} WHERE thread_id = $tid" ) );
}// end if
        return array(
                "status" => "ok",
                "message" => "Message Deleted!"
        );
}// end if/else
}  // end deleteMessage

 /*
   Saved in case BuddypressRead.php file gets overwritten
     */
    public function messages_get_messages() {
                $UserID = mysql_escape_string($_GET['UserID']);
                global $wpdb;
                global $bp;
        $this->init('messages');
        $oReturn = new stdClass();

        $aParams ['box'] = $this->box;
        $aParams ['per_page'] = $this->per_page;
        $aParams ['max'] = $this->limit;

        if (bp_has_message_threads($aParams)) {
            while (bp_message_threads()) {
                bp_message_thread();
                $aTemp = new stdClass();
                preg_match("#>(.*?)<#", bp_get_message_thread_from(), $aFrom);
                $oUser = get_user_by('login', $aFrom[1]);
                $aTemp->from[(int) $oUser->data->ID]->username = $aFrom[1];
                $aTemp->from[(int) $oUser->data->ID]->mail = $oUser->data->user_email;
                $aTemp->from[(int) $oUser->data->ID]->display_name = $oUser->data->display_name;
                preg_match("#>(.*?)<#", bp_get_message_thread_to(), $aTo);
                $oUser = get_user_by('login', $aTo[1]);
                $aTemp->to[(int) $oUser->data->ID]->username = $aTo[1];
                $aTemp->to[(int) $oUser->data->ID]->mail = $oUser->data->user_email;
                $aTemp->to[(int) $oUser->data->ID]->display_name = $oUser->data->display_name;
                $aTemp->subject = bp_get_message_thread_subject();
                $aTemp->excerpt = bp_get_message_thread_excerpt();
                $aTemp->link = bp_get_message_thread_view_link();
                                //$aTemp->message = bp_the_thread_message_content();
                                //$aTemp->debug = $wpdb->last_query;
                                $aTemp->id = bp_get_message_thread_id();                                
                                $id = $aTemp->id;
                                $message = $wpdb->get_row( $wpdb->prepare("SELECT * FROM {$bp->messages->table_name_messages} WHERE thread_id = $id ORDER BY date_sent DESC" ) );                 
                                $aTemp->message = $message->message;                                
                                
                                //$aTemp->debug        = $wpdb->last_query;


                $oReturn->messages [(int) bp_get_message_thread_id()] = $aTemp;
            }
        } else {
            return $this->error('messages');
        }
        return $oReturn;
    }// end get_messages
  
/******************************/
/******** SEND MESSAGE ********/
/******************************/
function sendMessage(){
	global $json_api;
	global $wpdb;
	global $bp;
	$from 		= mysql_escape_string($_GET['from']);
	$to		 	= mysql_escape_string($_GET['to']);
	$sub	 	= mysql_escape_string($_GET['sub']);	
	$mes	 	= mysql_escape_string($_GET['mes']);	

$args = array( 'recipients' => $to, 'sender_id' => $from, 'subject' => $sub, 'content' => $mes );	

messages_new_message( $args );
if($from && $to && $sub && $mes) {
return array(
"status" => "ok",
"message" => "Message Sent!"
);
}// end if we got all the parts
else {return array(
"status" => "ok",
"message" => "You need to provide from, to, subject(sub), and message(mes) content"
);
}
}// end sendMessage

/******************************/
/******** POST UPDATE *********/
/******************************/
function postUpdate(){
	global $json_api;
	global $wpdb;
	global $bp;

$user			= 1;
$year 			= date("Y");
$month			= date("m");
$filename		= $_FILES["file"]["name"];
$postContent 	= $_POST['postContent'];
$postUpload		= $_POST['postVideo'];
$url 			= 'http://www.shareatalent.com/wp-content/uploads/rtMedia/users/'.$user.'/'.$year.'/'.$month.'/'.$filename;
$ser_fol		= $_SERVER['DOCUMENT_ROOT'].'/wp-content/uploads/rtMedia/users/'.$user.'/'.$year.'/'.$month.'/';

move_uploaded_file($_FILES["file"]["tmp_name"],
$ser_fol . $_FILES["file"]["name"]);

if(!$_FILES) {
return array("status" 		=> "error",);
} else {
return array(
	"status" 		=> "ok",
	"url" 			=> $url,
	"server folder" => $ser_fol,
	"content" 		=> $postContent	
);
}
	
}// end postUpdate
  
}// end class
?>