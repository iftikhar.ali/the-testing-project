<?php
// https://developers.google.com/maps/documentation/javascript/examples/map-projection-simple

add_shortcode('distance_calc_sc','distance_calc');

function distance_calc() {

global $wpdb;
global $user_long_lat;
global $i;
global $map;
global $results;
global $uzip;
global $selected_zip;
global $results_page;
global $user_country;
global $zlookup;
global $search_keyword;
global $test_array;

$search_keyword[0] = NULL;

$current_user = wp_get_current_user();

$current_user->ID;

if(is_user_logged_in()){ 

$selected_zip = mysql_escape_string($_POST['zip']);

/*	User info */
// Get the current user's zip code
$user_zip	= $wpdb->get_results("
SELECT *
FROM `wp_bp_xprofile_data`
WHERE user_id = $current_user->ID
AND field_id = 36
LIMIT 1
");

// Check for a selected zip code
if(!$selected_zip){
	$uzip = $user_zip[0]->value;
}
elseif($selected_zip){
	$uzip = $selected_zip;
}

$user_country_query = $wpdb->get_results("
SELECT value
FROM `wp_bp_xprofile_data`
WHERE user_id = $current_user->ID
AND field_id = 42
LIMIT 1
");
$country = $user_country_query[0]->value;
$country_query = $wpdb->last_query;

// Get the longitude and latitude of current user's zip code
if($country == 'United States'){
$user_long_lat = $wpdb->get_results("
SELECT *
FROM `zipcodes`
WHERE postal_code =  $uzip
LIMIT 1
");
}

if($country == 'India'){
$user_long_lat = $wpdb->get_results("
SELECT *
FROM `zipcodes_india`
WHERE postal_code =  $uzip
LIMIT 1
");
}
if($country=='Germany'){
$user_long_lat = $wpdb->get_results("
SELECT *
FROM `zipcodes_germany`
WHERE postal_code =  $uzip
LIMIT 1
");
}

/* Primary Talent */
// Get the current user's primary talent
$user_primary_talent = $wpdb->get_results("
SELECT talent_id
FROM `sat_01_talent`
WHERE user_id = $current_user->ID
LIMIT 1
");

$ut = $user_primary_talent[0]->talent_id;

// Get the primary talent name for searching
$primary_talent_name = $wpdb->get_results("
SELECT *
FROM `sat_talents`
WHERE id = $ut
LIMIT 1
");

/* Second Talent */
$user_primary_talent2 = $wpdb->get_results("
SELECT talent_id
FROM `sat_02_talent`
WHERE user_id = $current_user->ID
LIMIT 1
");

$ut2 = $user_primary_talent2[0]->talent_id;

// Get the second talent name for searching
$second_talent_name = $wpdb->get_results("
SELECT *
FROM `sat_talents`
WHERE id = $ut2
LIMIT 1
");

/* Third Talent */
$user_primary_talent3 = $wpdb->get_results("
SELECT talent_id
FROM `sat_03_talent`
WHERE user_id = $current_user->ID
LIMIT 1
");

$ut3 = $user_primary_talent3[0]->talent_id;

// Get the second talent name for searching
$third_talent_name = $wpdb->get_results("
SELECT *
FROM `sat_talents`
WHERE id = $ut3
LIMIT 1
");

/* Fourth Talent */
$user_primary_talent4 = $wpdb->get_results("
SELECT talent_id
FROM `sat_04_talent`
WHERE user_id = $current_user->ID
LIMIT 1
");

$ut4 = $user_primary_talent4[0]->talent_id;

// Get the second talent name for searching
$fourth_talent_name = $wpdb->get_results("
SELECT *
FROM `sat_talents`
WHERE id = $ut4
LIMIT 1
");

/* Fifth Talent */
$user_primary_talent5 = $wpdb->get_results("
SELECT talent_id
FROM `sat_05_talent`
WHERE user_id = $current_user->ID
LIMIT 1
");

$ut5 = $user_primary_talent5[0]->talent_id;

// Get the second talent name for searching
$fifth_talent_name = $wpdb->get_results("
SELECT *
FROM `sat_talents`
WHERE id = $ut5
LIMIT 1
");

$sat1 	= $primary_talent_name[0]->level_3;
$sat2 	= $second_talent_name[0]->level_3;
$sat3 	= $third_talent_name[0]->level_3;
$sat4 	= $fourth_talent_name[0]->level_3;
$sat5 	= $fifth_talent_name[0]->level_3;

$selected_talent = mysql_escape_string($_POST['talent']);
if(!$selected_talent_click){ $search_bar_talent = mysql_escape_string($_GET['search']); }
if(!$search_bar_talent) {
$selected_talent = mysql_escape_string($_POST['talent']);
}
else {$selected_talent = $search_bar_talent; }

if($selected_talent){
$search_term = $selected_talent;
$search_for_the_talent_id = $wpdb->get_results("
SELECT id 
FROM  `sat_talents` 
WHERE  `level_3` =  '$search_term'
");
	$ut = $search_for_the_talent_id[0]->id;	
}

elseif (!$selected_talent) {$search_term =  $primary_talent_name[0]->level_3;}

if(!$selected_zip) { // Search groups with the talent in their keywords
$search_the_keyword = $wpdb->get_results("
SELECT user_id
FROM  `sat_01_talent` 
WHERE talent_id = $ut
");

/*
$m=0;
foreach ($search_the_keyword as $s) {
$id = $s->id;
$search_keyword_array = $wpdb->get_results("
		SELECT group_id as id
		FROM `sat_group_localization`
		WHERE group_id = $id		
		LIMIT 1
		");
	
	if(!empty($search_keyword_array)){	
	$search_keyword[$m]	= $search_keyword_array[0];
	$m++;
	}// end if
	
}// end foreach
*/

$search_keyword = $search_the_keyword;

} //end if

/* SELECTED ZIP */
if($selected_zip) { // Search groups with the talent in their keywords

	//look for a selected search term
	$selected_talent = mysql_escape_string($_POST['talent']);
	if($selected_talent){$search_term = $selected_talent;}
	elseif (!$selected_talent) {$search_term =  $primary_talent_name[0]->level_3;}

// Look up the search term
$search_the_keyword = $wpdb->get_results("
SELECT user_id
FROM  `sat_01_talent` 
WHERE talent_id = $ut
");

/*
// Build an array of the groups that have a search term match
$m=0;
foreach ($search_the_keyword as $s) {
$id = $s->user_id;
$search_keyword_array = $wpdb->get_results("
		SELECT group_id as id
		FROM `sat_group_localization`
		WHERE group_id = $id
		AND zip_code = $selected_zip
		LIMIT 1
		");
	
	if(!empty($search_keyword_array)){	
	$search_keyword[$m]	= $search_keyword_array[0];
	$m++;
	}// end if
	
}// end foreach
*/
$search_keyword = $search_the_keyword;
}// end if

$test_array = array();
$map = array();
$results = array();
$i=0;
foreach ($search_keyword as $sk) {

// lookup other group's zip codes
$g_id	=	$sk->user_id;
$other_zip	= $wpdb->get_results("
SELECT *
FROM `wp_bp_xprofile_data`
WHERE user_id = $g_id
and field_id = 36
LIMIT 1
");
$other_country	= $wpdb->get_results("
SELECT *
FROM `wp_bp_xprofile_data`
WHERE user_id = $g_id
and field_id = 42
LIMIT 1
");

$oz	=	$other_zip[$i]->value;
$oc = 	$other_country[$i]->value; // country lookup

// Get long/lat of the group's zip code
if ($oc == 'United States') {
$zlookup = $wpdb->get_results("
SELECT *
FROM `zipcodes` 
WHERE postal_code = $oz
");
}

elseif ($oc == 'India') {
$zlookup = $wpdb->get_results("
SELECT *
FROM `zipcodes_india` 
WHERE postal_code = $oz
");
}

elseif ($oc == 'Germany') {
$zlookup = $wpdb->get_results("
SELECT *
FROM `zipcodes_germany` 
WHERE postal_code = $oz
");
}

// Assign current user and group's long/lat
$point1['lat'] 		= $user_long_lat[0]->latitude;
$point1['long'] 	= $user_long_lat[0]->longitude;
$point2['lat'] 		= $zlookup[0]->latitude;
$point2['long'] 	= $zlookup[0]->longitude;
$point2['city'] 	= $zlookup[0]->place_name;
$point2['state'] 	= $zlookup[0]->place_name2;

	// Calculate the distance from current user to group's long/lat
    $distance = (3958 * 3.1415926 * sqrt(
    		($point1['lat'] - $point2['lat'])
    		* ($point1['lat'] - $point2['lat'])
    		+ cos($point1['lat'] / 57.29578)
    		* cos($point2['lat'] / 57.29578)
    		* ($point1['long'] - $point2['long'])
    		* ($point1['long'] - $point2['long'])
    	) / 180);

	// If the distance is under 55 miles, include the group
	if ($distance < 55 ) {
		$map[$i] = array(
			'o_id' 		=> $g_id,
			'o_long' 	=> $point2['long'],
			'o_lat' 	=> $point2['lat'],
			'o_city'	=> $point2['city'],
			'o_state'	=> $point2['state'],
			'o_zip'		=> $oz
		); 			
		// Build arrays for map and results
		$map_group[$i] = intval($oz);		
		$results[$i] = $g_id;
		$i++;
	}// end if 

} // end foreach

//var_dump($map);
//var_dump($results);

if(empty($map)){ ?>
	<h2>No results for your talent search. Try another!</h2>
	<form action="" method="post">
	<p class="groups-header"> 
		<select style="display:inline;" name="talent">	
			<option value="<?php echo $search_term;  ?>" selected="selected"><?php echo $search_term;  ?></option>
			<option>---------</option>
	    	<?php if($sat1){echo '<option value="' . $sat1 . '">' . $sat1. '</option>';} ?>
	    	<?php if($sat2){echo '<option value="' . $sat2 . '">' . $sat2. '</option>';} ?>
		    <?php if($sat3){echo '<option value="' . $sat3 . '">' . $sat3. '</option>';} ?>
	    	<?php if($sat4){echo '<option value="' . $sat4 . '">' . $sat4. '</option>';} ?>
    		<?php if($sat5){echo '<option value="' . $sat5 . '">' . $sat5. '</option>';} ?>
		</select>
	<input style="display:inline;" type="submit" value="Search"/>
</p>
	</form> 
<hr />
<?php } // end if 
if(!empty($map)) {

$map_group_clean_results	= array_unique($map_group);
$map_group_clean = array_filter($map_group_clean_results);
//var_dump($map_group_clean);
?>
<form action="" method="post">
<p class="groups-header">People in your area with a primary talent of  
<select style="display:inline;" name="talent">
	
	<option value="<?php echo $search_term;  ?>" selected="selected"><?php echo $search_term;  ?></option>
	<option>---------</option>
    <?php if($sat1){echo '<option value="' . $sat1 . '">' . $sat1. '</option>';} ?>
    <?php if($sat2){echo '<option value="' . $sat2 . '">' . $sat2. '</option>';} ?>
    <?php if($sat3){echo '<option value="' . $sat3 . '">' . $sat3. '</option>';} ?>
    <?php if($sat4){echo '<option value="' . $sat4 . '">' . $sat4. '</option>';} ?>
    <?php if($sat5){echo '<option value="' . $sat5 . '">' . $sat5. '</option>';} ?>
</select>
<input style="display:inline;" type="submit" value="Search"/>
</p>
</form> 
<hr />
    <style type="text/css">
body {
	font-family: sans-serif;
}

}
</style>

<script type="text/javascript">
google.maps.event.addDomListener(window, 'load', function() {
var map = new google.maps.Map(document.getElementById('map'), {
  zoom: 10,
  center: new google.maps.LatLng(<?php echo $point1['lat'] . ', ' . $point1['long'] ?>),
  mapTypeId: google.maps.MapTypeId.ROADMAP
});

var infoWindow = new google.maps.InfoWindow;

<?php $i=1; 
//var_dump($map_group_clean);
foreach ($map_group_clean as $mgc) {

$selected_talent = mysql_escape_string($_POST['talent']);
if($selected_talent){$if_selected_talent = '<input type="hidden" value="' .$selected_talent . '" name="talent" />';}

$z		= $mgc;

if ($country == 'United States'){
$lookup = $wpdb->get_results("
SELECT *
FROM `zipcodes` 
WHERE postal_code = $z
");
}
else if ($country == 'India') {
$lookup = $wpdb->get_results("
SELECT *
FROM `zipcodes_india` 
WHERE postal_code = $z
");
}
else if ($country == 'Germany') { 
$lookup = $wpdb->get_results("
SELECT *
FROM `zipcodes_germany` 
WHERE postal_code = $z
");
}

echo ' 	
		var onMarker' . $i . 'Click = function() {
		var marker = marker' . $i . ';
		var latLng = marker.getPosition();
		var city = \'' . $lookup[0]->place_name . '\' ;
		var st = \'' . $lookup[0]->place_name2 . '\' ;';
if(!$selected_zip) {
echo ' infoWindow.setContent(\'<div style="width:60%; float:left;"><form action="#" method="post"><input type="hidden" value="' . $lookup[0]->postal_code . '" name="zip" /><input type="submit" value="Find Talent in ' . $lookup[0]->place_name . ', ' . $lookup[0]->place_name2 . '!" />' . $if_selected_talent . '</form></div>\');';
echo ' infoWindow.open(map, marker' . $i . ');';
};
echo '};';
echo '';
$i++;
}?>

google.maps.event.addListener(map, 'click', function() {
  infoWindow.close();
});
<?php 
$i=1;
foreach ($map_group_clean as $mgc) {
$z		= $mgc;

if ($user_country == 'United States') {
$lookup = $wpdb->get_results("
SELECT *
FROM `zipcodes` 
WHERE postal_code = $z
");
}

elseif ($user_country == 'India') {
$lookup = $wpdb->get_results("
SELECT *
FROM `zipcodes_india` 
WHERE postal_code = $z
");
}

elseif ($user_country == 'Germany') {
$lookup = $wpdb->get_results("
SELECT *
FROM `zipcodes_germany` 
WHERE postal_code = $z
");
}

echo 'var marker' . $i . ' = new google.maps.Marker({
  map: map,
  position: new google.maps.LatLng(' . $lookup[0]->latitude . ', ' . $lookup[0]->longitude . ')
  
});
';
$i++;
} // end foreach var marker
$i=1;
foreach ($map_group_clean as $mgc) {
$z		= $mgc;
$lookup = $wpdb->get_results("
	SELECT *
	FROM `zipcodes`
	WHERE postal_code = $z
	LIMIT 1	
");
echo 'google.maps.event.addListener(marker' . $i . ', \'click\', onMarker' . $i . 'Click);';
$i++;
} // end foreach addListener

?>
  });
</script>	
   
    <div id="map-container">
    <div id="map"></div>
<?php 

/*******************************************/
/********* Results container area **********/
/*******************************************/

?>    
    <div id="results-container">
<?php	
	$results_page = intval(mysql_escape_string($_POST['results-page'])); 
	if(empty($results_page)){$results_page_up = 2;}
	if(!empty($results_page)){$results_page_up = $results_page+1; $results_page_down = $results_page-1;}	
?>
<?php //var_dump($results); ?>
<?php if(!$selected_zip){ ?>
    <h4>Results - <?php if( (count($map)/4) > 1 ){ 
	if(empty($results_page)){
	echo ' Page 1/' . ceil(count($map)/4); 
	}
	else {
	echo ' Page '. $results_page .'/' . ceil(count($map)/4); 
		}
?>	
</h4>
<?php
	//PREVIOUS button
	if($results_page==2){
		echo '<a href="http://www.shareatalent.com/find-local-talent" style="border: none; background: none; font-size: 16px; font-weight: bold; color: #e4078c;" >
		<< Previous </a>';
	}
	elseif($results_page>=3){
		echo '<form style="display:inline;" id="group-page" action="" method="post">
			<input type="hidden" name="results-page" value="'. $results_page_down . '" />
			<input style="border: none; background: none; font-size: 16px; font-weight: bold; color: #e4078c;" type="submit" value="<< Previous" /></form>';
	}
	// NEXT button
	if(intval($results_page) !== intval(ceil(count($map)/4) ) ){
	echo '<form style="display:inline;" id="group-page" action="" method="post">
		<input type="hidden" name="results-page" value="'. $results_page_up . '" />
		<input style="border: none; background: none; font-size: 16px; font-weight: bold; color: #e4078c;" type="submit" value="Next >>" /></form>';
	}//end if
	}
} //end if

elseif($selected_zip) { ?>
	<h4>Results for <?php echo $lookup[0]->place_name . ', ' . $lookup[0]->place_name2; ?>: 
	<?php 	
	if(empty($results_page)){ ?>
		<br/> Page 1/<?php echo ceil(count($map)/4); 
		} 
	elseif(!empty($results_page)) {?><br /> Page <?php echo $results_page .'/' . ceil(count($map)/4); 
		}
?> 
</h4> 
<?php 
	//PREVIOUS button
	if($results_page==2){
		echo '<form style="display:inline;" id="group-page" action="" method="post">
			<input type="hidden" value="' . $lookup[0]->postal_code . '" name="zip" />
			<input style="border: none; background: none; font-size: 16px; font-weight: bold; color: #e4078c;" type="submit" value="<< Previous " /></form>';
			// end if';
		}
	elseif($results_page>=3){
		echo '<form style="display:inline;" id="group-page" action="" method="post">
			<input type="hidden" name="results-page" value="'. $results_page_up . '" />
			<input style="border: none; background: none; font-size: 16px; font-weight: bold; color: #e4078c;" type="submit" value="Next >>" /></form>';
		}
	// NEXT button
	if(intval($results_page) !== intval(ceil(count($map)/4) ) ){
	echo '<form style="display:inline;" id="group-page" action="" method="post">
		<input type="hidden" value="' . $lookup[0]->postal_code . '" name="zip" />
		<input type="hidden" name="results-page" value="'. $results_page_up . '" />
		<input style="border: none; background: none; font-size: 16px; font-weight: bold; color: #e4078c;" type="submit" value="Next >>" /></form>';
	}//end if
	?>
    
<?php } //end else ?>
<?php
$rp=1;

if($results_page){
// replace with equation $split = ($results_page * 4) - 4;
if($results_page == 	2	) {$split =	4	; }
if($results_page == 	3	) {$split =	8	; }
if($results_page == 	4	) {$split =	12	; }
if($results_page == 	5	) {$split =	16	; }
if($results_page == 	6	) {$split =	20	; }
if($results_page == 	7	) {$split =	24	; }
if($results_page == 	8	) {$split =	28	; }
if($results_page == 	9	) {$split =	32	; }
if($results_page == 	10	) {$split =	36	; }
if($results_page == 	11	) {$split =	40	; }
if($results_page == 	12	) {$split =	44	; }
if($results_page == 	13	) {$split =	48	; }
if($results_page == 	14	) {$split =	52	; }
if($results_page == 	15	) {$split =	56	; }
if($results_page == 	16	) {$split =	60	; }
if($results_page == 	17	) {$split =	64	; }
if($results_page == 	18	) {$split =	68	; }
if($results_page == 	19	) {$split =	72	; }
if($results_page == 	20	) {$split =	76	; }
if($results_page == 	21	) {$split =	80	; }
if($results_page == 	22	) {$split =	84	; }
if($results_page == 	23	) {$split =	88	; }
if($results_page == 	24	) {$split =	92	; }
if($results_page == 	25	) {$split =	96	; }

$split_plus = $split + 3;

// Break results up into multiple pages for paging of 
$r_sliced = array_slice($results,$split,$split_plus,true);

foreach ($r_sliced as $r) { 

$ou 		= new WP_User($r);
$ou_n		= $ou->display_name;
$ou_s		= $ou->user_nicename;
//$ou_t		= strip_tags($ou->description);
$ou_t_array		= $wpdb->get_results("
SELECT about_talent
FROM  `sat_about_talent` 
WHERE user_id = $r
LIMIT 1
");
//var_dump($ou_t_array);
$ou_t = stripslashes($ou_t_array[0]->about_talent);
$ou_tc		= strpos($ou_t,$search_term);

//check if the search term is at the very beginning of the results
if($ou_tc<30){$ou_tr		= substr($ou_t, $ou_tc,100);}
else {$ou_tr	= substr($ou_t, $ou_tc-30,100);}

$st_html	= '<span style="background-color:#e4078c; color:#fff;padding:2px 3px 2px 3px;">' . $search_term . '</span>';
$ou_final	= str_ireplace($search_term,$st_html,$ou_tr);
?>
<hr />
<h4><a href="http://shareatalent.com/groups/<?php echo $ou_s; ?>" target="_blank"><?php echo $ou_n; ?></a></h4>
<p>...<?php echo $ou_final; ?>...</p>
<?php } //end foreach 

} // end if ?>

<?php
if(empty($results_page)){

$r_sliced = array_slice($results,0,4,true);
foreach ($r_sliced as $r) { 
//echo '<p>No page</p>';
//$map_array = array($m);
//$m_sliced = array_slice($map_array,0,4);
//echo $map_array[0]['o_id'];
$ou 		= new WP_User($r);
$ou_n		= $ou->display_name;
$ou_s		= $ou->user_nicename;
//$ou_t		= strip_tags($ou->description);
$ou_t_array		= $wpdb->get_results("
SELECT about_talent
FROM  `sat_about_talent` 
WHERE user_id = $r
LIMIT 1
");
//var_dump($ou_t_array);
$ou_t = stripslashes($ou_t_array[0]->about_talent);
$ou_tc		= strpos($ou_t,$search_term);
//var_dump($ou);
//check if the search term is at the very beginning of the results
if($ou_tc<30){$ou_tr		= substr($ou_t, $ou_tc,100);}
else {$ou_tr	= substr($ou_t, $ou_tc-30,100);}

$st_html	= '<span style="background-color:#e4078c; color:#fff;padding:2px 3px 2px 3px;">' . $search_term . '</span>';
$ou_final	= str_ireplace($search_term,$st_html,$ou_tr);
?>
<hr />
<h4><a href="http://www.shareatalent.com/members/<?php echo $ou_s; ?>" target="_blank"><?php echo $ou_n; ?></a></h4>
<p>...<?php echo strip_tags($ou_final); ?>...</p>

<?php
$rp++;
}//end if r
 } //end foreach
	
	 ?>
		<hr />
    </div>
    
    </div><!-- end map-container -->


<?php

} // end if (!empty($map)
/***************/

} 
else {echo '<h2>You must be <a href="http://shareatalent.com/wp-login.php">Logged In</a> to see this page</h2>'; }
// end if is_user_logged_in 

} // end distanc_calc
?>