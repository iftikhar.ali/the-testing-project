<?
$location = $_SERVER['DOCUMENT_ROOT'];
require_once($location . '/wp-load.php'); 
require_once($location . '/wp-config.php'); 
global $wpdb; 
global $bp;
global $rtmedia_query;

$user_id = $_GET['userid'];
$title = $_GET['title'];

$post_vars = array(
            'post_title' => $title,
            'post_type' => 'rtmedia_album',
            'post_author' => $user_id,
            'post_status' => 'hidden'
        );
$album_id = wp_insert_post ( $post_vars );
if($album_id){

$current_album = get_post ( $album_id, ARRAY_A );
        if ( $context === false ) {
            $context = (isset ( $rtmedia_interaction->context->type )) ? $rtmedia_interaction->context->type : NULL;
        }
        if ( $context_id === false ) {
            $context_id = (isset ( $rtmedia_interaction->context->id )) ? $rtmedia_interaction->context->id : NULL;
        }
        // add in the media since album is also a media
        //defaults
        global $rtmedia_interaction;		
        $attributes = array(
            'blog_id' => get_current_blog_id (),
            'media_id' => $album_id,
            'album_id' => NULL,
            'media_title' => $current_album[ 'post_title' ],
            'media_author' => $current_album[ 'post_author' ],
            'media_type' => 'album',
            'context' => $context,
            'context_id' => $user_id,
            'activity_id' => NULL,
            'privacy' => NULL
        );
	$newAlbum->media = new RTMediaMedia();
	$attributes = apply_filters("rtmedia_before_save_album_attributes",$attributes, $_POST);
        $rtmedia_id = $newAlbum->media->insert_album ( $attributes );
        $rtMediaNav = new RTMediaNav();
		$media_count = $rtMediaNav->refresh_counts ( $context_id, array( "context" => $context, 'media_author' => $context_id ) );
        /* action to perform any task after adding the album */

	global $rtmedia_points_media_id;
	$rtmedia_points_media_id = $rtmedia_id;
        do_action ( 'rtmedia_after_add_album', $newAlbum );

//        return $rtmedia_id;
echo json_encode(array(
	'status' => 'ok',
	'rtmedia_id' => $rtmedia_id
	));






} else {
	echo json_encode(array(
		'status' => 'error',
		'message' => 'Meh. We broke something.'
	));
	}// else