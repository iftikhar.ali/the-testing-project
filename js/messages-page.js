document.addEventListener('deviceready', MessagesFunction, false);
function MessagesFunction() { 
cl("MessagesFunction ready!");
jQuery("messageContent").trigger("create");
jQuery("messageTo").trigger("create");
jQuery("messageSubject").trigger("create");
var msgSub = '';
jQuery("a[id^='messages-menu']").on("vclick",function($) 
{
jQuery("#friends-content-list").empty();
var UserID = window.localStorage.getItem("satUserID");	
var UserName = window.localStorage.getItem("satUserName");	
	if (UserID>0) 
	{		
		var FriendsURL = 'http://www.shareatalent.com/api/buddypressread/friends_get_friends/?username='+UserName;
			cl("FriendsURL: "+FriendsURL);
			jQuery.getJSON(FriendsURL,
			function(FriendsList){
					var status = FriendsList.status;
					if (status == 'error'){jQuery("#friends-content-list").html('<li><a href="#search-page">No friends found. Go find some!</a></li>').trigger("create").listview("refresh");}
					else {
					jQuery("#messageTo").html("");	
					jQuery.each(FriendsList.friends, function (result, value){	
						jQuery("#messageTo").append(
						'<option value="'+result+'">'+value.display_name+'</option>');
						jQuery("#messageTo").trigger("create");
					}); // end each
					}// end else
				});	
	}// end if actual user

// Fix for Android focus
jQuery('#messageSubject').on("focusin", function(){
		console.log("messageSubject field adjusted for android")
		jQuery("#messages-content").css("height", "1200px");
		jQuery('html, body').animate({
			scrollTop: jQuery('#messageSubject').offset().top - 20		
			});
	});
	jQuery('#messageSubject').on("focusout", function(){
		jQuery("#messages-content").css("height", "100%");
		});
jQuery('#messageContent').on("focusin", function(){
		console.log("messageSubject field adjusted for android")
		jQuery("#messages-content").css("height", "1200px");
		jQuery('html, body').animate({
			scrollTop: jQuery('#messageContent').offset().top - 20		
			});
	});
	jQuery('#messageContent').on("focusout", function(){
		jQuery("#messages-content").css("height", "100%");
		});

var UserID = parseInt(window.localStorage.getItem("satUserID"));	
	if (UserID>0) 
	{		
		cl('User# '+UserID + " clicked the Messages Page menu");
			var rndm = Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000; // +'&r='+rndm
			var messageURL = 'http://shareatalent.com/api/get_messages.php?user='+UserID+'&box=inbox';
				cl(messageURL);
				var m = 1;
			jQuery.getJSON(messageURL,function(data){
				console.log("Message: "+data);
				messageURLSuccess(data);
				});

/*
jQuery.ajax({
	type: 'GET',
	dataType: 'jsonp',
	cache : false,
	url: messageURL,
	success: function(data) { cl("Message Ajax Success!"); messageURLSuccess(data) },
	fail: messageURLFail
 });// end Ajax
*/			
function messageURLFail(data, textStatus, jqXHR) {
	var rndm = Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000; // +'&r='+rndm
	var messageURL = 'http://shareatalent.com/api/get_messages.php?user='+UserID+'&box=inbox';
	jQuery.getJSON(messageURL,
	function(userMessages){
			jQuery("#messages-content-list").append('<h2>No Messages to display</h2>');
	}); 
}// end messageURLFail

function messageURLSuccess(data, textStatus, jqXHR) {
// Messages
	cl("messageURLSuccess Called!");
	cl(JSON.stringify(data));
	if(data.status != 'error'){
	jQuery.each(data.messages, function (msgID, msgObj ){	
		cl("Message #"+m);
		m++;
		
		jQuery("#messages-content-list").append(						
		'<div class="message-container" id="message-id-'+msgObj.thread_id+'" data-role="collapsible">'+
		'<h3>From: '+msgObj.from[0].username+
		' | Subj: <span id="message-sub-'+msgObj.id+'">'+msgObj.subject+'</span></span><br />'+
		'<span class="message-excerpt" style="font-size:12px;font-style:italic;">'+msgObj.excerpt+
		'</span></h3><!-- /header -->'+
		'<div class="message-content">'+
		'<div class="message-detail"> <div class="message-from-avatar">'+msgObj.message_avatar+'</div>'+msgObj.excerpt+'</div>'+
		'<div class="reply-to-message"><p>Reply: </p><textarea id="reply-message-'+msgObj.thread_id+'"></textarea></div>'+
		'<div class="message-options">'+
			'<div style="width:40%;float:left;display:inline-block;"><button id="reply-to-message-'+msgObj.thread_id+'">Send Reply</button></div>'+
			'<div style="width:40%;float:right;display:inline-block;">'+
			  '<button id="delete-this-message-'+msgObj.thread_id+'" name="'+msgObj.thread_id+'" data-theme="e">Delete</button></div>'+
			'</div><!-- /message-options -->'+
			'</div><!-- /message-content -->'+
			'</div><!-- /message-container -->'
		).trigger("create");
		jQuery("#messages-content-list").trigger("create");
		cl("messages-content-list listview created");
		});// end each
	}// end if
	else {
		jQuery("#messages-content-list").html('<p style="color:red;">No messages right now.</p>');
		}
		deleteMessage();
		replyToMessage();
		getYourFriends();

}
				};		
		}); 
}; // end message

	// Populate list of friends for sending messages
function getYourFriends() {
	cl("getYourFriends function called!");
	var UserID = window.localStorage.getItem("satUserID");	
	var UserName = window.localStorage.getItem("satUserName");	
	if (UserID>0) 
	{		
		var rndm = Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000; // +'&r='+rndm
		var FriendsURL = 'http://www.shareatalent.com/api/buddypressread/friends_get_friends/?username='+UserName+'&r='+rndm;
			cl("FriendsURL: "+FriendsURL);
			jQuery.getJSON(FriendsURL,
			function(FriendsList){
					var status = FriendsList.status;
					cl("FriendsList "+JSON.stringify(FriendsList))
					if (status == 'error'){jQuery("#messages-messages").html('<h2><a href="#search-page">No friends found. Go find some!</a></h2>');}
					else {
					jQuery("#messageTo").html("");	
					jQuery.each(FriendsList.friends, function (result, value){	
						jQuery("#messageTo").append(
						'<option value="'+result+'">'+value.display_name+'</option>');
						jQuery("#messageTo").trigger("create");
					}); // end each
					jQuery("#messageTo").trigger("create");
					}// end else
				});	
	}// end if actual user
// End populate friend list
					}// end else	

// Send message
jQuery("#message-send-button").on("vclick",function(){
	var UserID = window.localStorage.getItem("satUserID");	
	var messageTextSubject = encodeURIComponent(jQuery('#messageSubject').val());
	var messageTextContent = encodeURIComponent(jQuery('textarea#messageContent').val());
	var messageSentTo = jQuery("#messageTo option:selected").val();
	if(!messageTextSubject && !messageTextContent){navigator.notification.alert(
		"Message subject & Message content should not be empty!",
		"Share A Talent", 
		"Close")}// end if no post content
	else{
	cl('Send button pressed with Subject: '+messageTextSubject +' Message: '+messageTextContent+' To: ' +messageSentTo+' From: '+UserID);
	var sendMessageURL = 'http://www.shareatalent.com/api/usersettings/sendMessage/?to='+messageSentTo+'&from='+UserID+'&sub='+messageTextSubject+'&mes='+messageTextContent
	cl(sendMessageURL);
	// Ajax for sending a new message
	jQuery.ajax({
		url: sendMessageURL,
		dataType: "json",
		crossDomain: true,
		success:
		function(json) {      
			console.log(json);
				
		var message = json.message;										
		cl('Message: '+ message);	
		jQuery("#messages-messages").html('<h2 style="color:red;">'+message+'</h2>').delay(20).slideDown("slow");
		jQuery("#messages-messages").html('<h2 style="color:red;">'+message+'</h2>').delay(2000).slideUp("slow");
		jQuery("#new-message").trigger( "collapse" );
		jQuery('#messageSubject').val( "" );
		jQuery('textarea#messageContent').val( "" );
		
		}// end success
		}); // end Ajax
	}
});// end send new message



// Delete button called after messages are generated
// Delete a Message
function deleteMessage(){
jQuery("button[id^=delete-this-message]").on("vclick",function(){
	var UserID = window.localStorage.getItem("satUserID");	
	var deleteID = this.id.replace("delete-this-message-", "");
	var deleteTID = jQuery(this).attr("name");
	cl('button TID: '+deleteTID+' For UserID: '+UserID);
	var deleteMessageURL = "http://www.shareatalent.com/api/usersettings/deleteMessage/?messageID="+deleteID+"&UserID="+UserID+"&tid="+deleteTID;
	cl(deleteMessageURL);
	jQuery.getJSON(deleteMessageURL, function(deleteThisMessage){
		var status = deleteThisMessage.status;
		cl("Delete Message Status: "+status);
		if(status == "error") {cl(deleteThisMessage)}
		if(status == "ok") {
			cl(deleteThisMessage)
			cl('#mesage-id-'+deleteID)
			jQuery('#message-id-'+deleteID).slideUp("slow");	
			jQuery("#messages-messages").html('<h2 name="'+deleteID+'" style="color:red;">'+deleteThisMessage.message+'</h2>').slideDown("fast").delay(2000).slideUp("slow");
			}
		
		
		});
});
}// end delete button

// Reply button called after messages are generated
// Reply to a Message
function replyToMessage(){
jQuery("button[id^=reply-to-message]").on("vclick",function(){
	var UserID 		= window.localStorage.getItem("satUserID");	
	var replyID 	= this.id.replace("reply-to-message-", "");
	var replySub	= encodeURIComponent(jQuery("#message-sub-"+replyID).text());
	var replyBase 	= jQuery("textarea#reply-message-"+replyID).val().replace("'", "&#39;");
	cl("replyBase: "+replyBase);
	var replyMsg 	= encodeURIComponent(replyBase);
	cl('Reply ID: '+replyID+' For UserID: '+UserID+' Message: '+replyMsg+' Subject: '+replySub);
	var replyMessageURL = "http://www.shareatalent.com/api/usersettings/replyToMessage/?tid="+replyID+"&sub="+replySub+"&mes="+replyMsg+"&callback?";
	cl(replyMessageURL);
	jQuery.ajax({
		url: replyMessageURL,
		type: "PUT",
		crossDomain: true,
		dataType: "jsonp",
		async: true,
		success: function(data){
		jQuery('#message-id-'+replyID).trigger( "collapse" );	
		jQuery("#messages-messages").html('<h2 name="'+replyID+'" style="color:red;">'+data.message+'</h2>')			.slideDown("fast").delay(2000).slideUp("slow");
		}// end success
		}); // end ajax
/*	
	jQuery.getJSON(replyMessageURL, function(ReplyToMessage){
		var status = ReplyToMessage.status;
		var message = ReplyToMessage.message;
		cl("Reply To Message Status: "+status);
		cl("Reply To Message : "+message);
		if(status == "error") {cl(ReplyToMessage)}
		if(status == "ok") {
			cl(ReplyToMessage)
			jQuery('#message-id-'+replyID).trigger( "collapse" );	
			jQuery("#messages-messages").html('<h2 name="'+replyID+'" style="color:red;">'+message+'</h2>').slideDown("fast").delay(2000).slideUp("slow");
			}		
		});
	*/
});
}// end delete button
