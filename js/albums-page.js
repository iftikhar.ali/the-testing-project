document.addEventListener('deviceready', albumCreate, false);
function albumCreate(){
	cl("Ready to create an album");
jQuery("#create-album-button").on("click",function(){
	cl("create album button clicked");
	jQuery("#cancel-create-album-button-container").css("display", "block");	
	jQuery("#create-album-button-container").css("display", "none");
	jQuery("#create-album-inputs").slideDown("fast");	
	});
	
jQuery("#cancel-create-album-button").on("click",function(){
	cl("Cancel album button clicked")
	jQuery("#create-album-button-container").css("display", "block");
	jQuery("#cancel-create-album-button-container").css("display", "none");
	
	jQuery("#create-album-inputs").slideUp("fast");
	jQuery("#new-album-name").val('');
	});

jQuery('#save-album-name').on('click', function(e) {
        var albumName = jQuery.trim(jQuery('#new-album-name').val());
		var privacy = jQuery('#album-privacy').val();
        var context_id = window.localStorage.getItem("satUserID");
		if (albumName == '') {
            navigator.notification.alert("Album name is missing","Share A Talent","Please enter");
		} else {
			var createAlbumURL = 'http://www.shareatalent.com/api/create_album.php?user_id='+context_id+'&title='+albumName+'&privacy='+privacy+'&context=profile';
			console.log(createAlbumURL);
			jQuery.getJSON(createAlbumURL,function(d){
				console.log('Album Created!')
				console.log(d)
				jQuery("#create-album-button-container").css("display", "block");
				jQuery("#cancel-create-album-button-container").css("display", "none");
				jQuery("#create-album-inputs").slideUp("fast");
				jQuery("#new-album-name").val('');
			});
		}
	});//
/*
var ajaxurl = 'http://www.shareatalent.com/wp-admin/admin-ajax.php';
jQuery.post(ajaxurl, data, function(response) {
		response = response.trim();
		console.log("Create Album Response: ");
		console.log(response);
		});
        } else {
            alert('You must give your album a name!');
        }

		reloadAlbumsPage();
    });
*/
/*
jQuery("#save-album-name").on("vclick",function(){
	var newAlbumName = jQuery("#new-album-name").val();
	if(!newAlbumName){
		var message = "You need a name for your album!";
		navigator.notification.alert(message, function(){}, "Share A Talent says: ", "Ok, I'll fix it...");
		} else {	
		cl("New album name: "+newAlbumName);
		jQuery("#create-album-button-container").css("display", "block");
		jQuery("#cancel-create-album-button-container").css("display", "none");
		jQuery("#create-album-inputs").slideUp("fast");
		jQuery("#new-album-name").val('');
		var UserID = window.localStorage.getItem("satUserID");
		var newAlbumURL = 'http://www.shareatalent.com/api/usersettings/createAlbum/?userid='+UserID+'&title='+newAlbumName
		jQuery.getJSON(newAlbumURL, function(newAlbumResults){
			cl("newAlbumResults: ");
			console.log(newAlbumResults).fail(console.log("Something didn't work right"));
			}// end function
		
		).complete(reloadAlbumsPage());// end getJSON
			
		}// else
		
	});
	
function reloadAlbumsPage(){  
cl("Reloading Albums");
jQuery.mobile.loading('show');
jQuery("#albums-content-list").html('');
var UserID = window.localStorage.getItem("satUserID");	
var UserName = window.localStorage.getItem("satUserName");	
		var albumsURL = 'http://www.shareatalent.com/members/'+UserName+'/media/album/?json';
			cl("albumsURL: "+albumsURL);				
			jQuery.getJSON(albumsURL,
			function(albumsList){
					cl("albumsList: "+albumsList.data);
					jQuery.each(albumsList.data, function (result, value){	
						jQuery("#albums-content-list").append(
						'<div data-role="button" id="album-id-'+value.id+'">'+value.media_title+'</div>');
						jQuery("#albums-content-list").trigger("create").listview("refresh");		
					});		
					jQuery.mobile.loading('hide');
				});
}// end reloadAlbumsPage
*/
}// end albumsPage