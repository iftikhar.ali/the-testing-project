function loadMyGroupActivities(){
var UserID = window.localStorage.getItem("satUserID");
jQuery.mobile.loading('show');
var pageNumber =1;
var getActivitiesURL = 'http://www.shareatalent.com/api/get_group_activities.php?group_id'+;
jQuery.getJSON(getActivitiesURL,function(d){
	if(d.activities.length > 0){
		var n=0;
		jQuery("#list-of-my-group-recent-activity").empty();
		jQuery.each(d.activities,function(results,values){
			console.log(values);
			var todaysDate = new Date(values.date_recorded);
			var diff = Math.abs(new Date() - todaysDate);
			var ms = diff;
			var hour = Math.floor((ms/1000/60/60));
			var min = Math.floor((ms/1000/60)%60);
			var sec = Math.floor((ms/1000) % 60);
			
			var timeString = ((hour)?(' '+hour+' hours '):'')+((min)?(' '+min+' minutes '):'');
			if(timeString==='') {timeString = ' '+sec+' seconds';}
			var appendContents = '<div class="activity-content">'+
		'<div class="activity-header">'+
			'<p><a href="http://www.shareatalent.com/members/'+values.user_login+'/" title="'+values.user_login+'">'+values.action+'<span class="time-since">'+timeString+' ago</span></a></p>'+
		'</div>'+
			'<div class="activity-inner">'+
				'<p>'+values.content+'</p>'+
			'</div>'+
	'</div>	'+
'</li>';
			
			jQuery("#list-of-my-group-recent-activity").append(appendContents);
			jQuery("#list-of-my-group-recent-activity").trigger("create").listview("refresh");
			
			})// each
		}// if status ok
	if(d.status == 'error'){
		var n=0;		
			console.log(d.message);
			jQuery("#list-of-my-group-recent-activity").append('<li style="list-style:none; font-size:14px; font-weight:normal; border-top:1px solid #ccc;" class="ui-li ui-li-static">'+d.message+'</li>');
			jQuery("#list-of-my-group-recent-activity").trigger("create").listview("refresh");
			n++;			
		}// if status
	jQuery("#load-more-group-activities").attr('current-page-number',(parseInt(pageNumber)+1));
	});// getJSON
	jQuery.mobile.loading('hide');
}//loadMyGroupActivities

jQuery("#load-more-group-activities").on("vclick",function(event){
jQuery.mobile.loading('show');
var pageNumber = jQuery("#load-more-group-activities").attr('current-page-number');
var UserID = window.localStorage.getItem("satUserID");
	var getActivitiesURL = 'http://www.shareatalent.com/api/get_group_activities.php?group_id='+;
jQuery.getJSON(getActivitiesURL,function(d){
	if(d.activities.length > 0){
	jQuery("#load-more-group-activities").attr('current-page-number',(parseInt(pageNumber)+1));
		jQuery.each(d.activities,function(results,values){
			console.log(values);
			var todaysDate = new Date(values.date_recorded);
			var diff = Math.abs(new Date() - todaysDate);
			var ms = diff;
			var hour = Math.floor((ms/1000/60/60));
			var min = Math.floor((ms/1000/60)%60);
			var sec = Math.floor((ms/1000) % 60);
			
			var timeString = ((hour)?(' '+hour+' hours '):'')+((min)?(' '+min+' minutes '):'');
			if(timeString==='') {timeString = ' '+sec+' seconds';}
			var appendContents = '<div class="activity-content">'+
		'<div class="activity-header">'+
			'<p><a href="http://www.shareatalent.com/members/'+values.user_login+'/" title="'+values.user_login+'">'+values.action+'<span class="time-since">'+timeString+' ago</span></a></p>'+
		'</div>'+
			'<div class="activity-inner">'+
				'<p>'+values.content+'</p>'+
			'</div>'+
	'</div>	'+
'</li>';
			
			jQuery("#list-of-my-group-recent-activity").append(appendContents);
			jQuery("#list-of-my-group-recent-activity").trigger("create").listview("refresh");
			
			})// each
		}// if status ok
	if(d.status == 'error'){
		var n=0;		
			console.log(d.message);
			jQuery("#list-of-my-group-recent-activity").append('<li style="list-style:none; font-size:14px; font-weight:normal; border-top:1px solid #ccc;" class="ui-li ui-li-static">'+d.message+'</li>');
			jQuery("#list-of-my-group-recent-activity").trigger("create").listview("refresh");
			n++;			
		}// if status
	});// getJSON
jQuery.mobile.loading('hide');
});