document.addEventListener('deviceready', getFriendsPageFunction, false);
function getFriendsPageFunction(){
cl("Friends Page Ready!");
jQuery("a[id^='friends-page-menu'], #main-profile-friends-link").on("vclick",function($) 
{
jQuery("#friends-content-list-ul").empty();
var UserID = window.localStorage.getItem("satUserID");	
var UserName = window.localStorage.getItem("satUserName");	
	if (UserID>0) 
	{		
		var FriendsURL = 'http://www.shareatalent.com/api/buddypressread/friends_get_friends/?username='+UserName;
			cl("FriendsURL: "+FriendsURL);
			jQuery.getJSON(FriendsURL,
			function(FriendsList){
					var status = FriendsList.status;
					if (status == 'error'){jQuery("#friends-content-list").html('<li><a href="#search-page">No friends found. Go find some!</a></li>').trigger("create").listview("refresh");}
					else {
					jQuery.each(FriendsList.friends, function (result, value){	
						jQuery("#friends-content-list-ul").append(
						'<li><a href="#member-page?memberId='+result+'" id="memberId-'+result+'">'+value.display_name+'</a></li>');
						jQuery("#friends-content-list-ul").trigger("create").listview("refresh");
					}); // end each
					}// end else
				});	
	}// end if actual user
	}); // end function
jQuery.mobile.loading('hide');
} // end function