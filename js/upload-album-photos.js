document.addEventListener('deviceready', albumUploadImage, false);
function albumUploadImage(){
	cl("Ready to upload photo into album");
	jQuery("#upload-album-image-button").on("click",function(){
		cl("upload image button button clicked");
		jQuery("#cancel-upload-album-image-button-container").css("display", "block");	
		jQuery("#upload-album-image-button-container").css("display", "none");
		jQuery("#upload-album-image-inputs").slideDown("fast");	
	});
		
	jQuery("#cancel-upload-album-image-button").on("click",function(){
		cl("Cancel upload album image button clicked");
		jQuery("#upload-album-image-button-container").css("display", "block");
		jQuery("#cancel-upload-album-image-button-container").css("display", "none");
		jQuery("#upload-album-image-inputs").slideUp("fast");
	});
	
	jQuery('#save-album-image').on('click', function(e) {
		console.log('save button clicked');
        var hasPhoto = jQuery("#file-to-upload-album-image").val();
        var context_id = window.localStorage.getItem("satUserID");
		if(hasPhoto){
			    console.log('has photo');
				uploadPhotoToAlbum();
				jQuery("#upload-album-image-button-container").css("display", "block");
				jQuery("#cancel-upload-album-image-button-container").css("display", "none");
				jQuery("#upload-album-image-inputs").slideUp("fast");
			}
	});//	
	
}
jQuery("#file-to-upload-album-image").on("change",function(event){	
	
	var files = [];
	jQuery.each(event.target.files, function(index, file) {
    var reader = new FileReader();
	
	
    reader.onload = function(event) { 
		
      object = {};
      object.filename = file.name;
      object.data = event.target.result;
      files.push(object);
	  console.log("object.data:");
	  console.log(object.data);
	  window.localStorage.setItem("satAlbumImageObject",object.data);
	  
    };  
    reader.readAsDataURL(file);
  });
});// file input change
	
function uploadPhotoToAlbum(){
	console.log("New Upload Process: ");	
	var passToken = window.localStorage.getItem("satpass");
	var passUserName = window.localStorage.getItem("satUserName");
	var tokenURL = 'http://www.shareatalent.com/wp-admin/admin-ajax.php';
	var tokenData = {
			action		: "rtmedia_api",
			method		: "wp_login",
			username  	: passUserName,
			password  	: passToken
		};
	jQuery.post(tokenURL,tokenData,function(t){
		console.log("Token created");
		console.log(t);
		tp = JSON.parse(t);
		console.log("Parsed Token:"+tp.data.access_token);
		token = tp.data.access_token;
		window.localStorage.setItem("satToken",token);
		console.log("SAtToken: ");
		console.log(tp.data.access_token);
		console.log("--------------");
		sendFileToAlbum();
	}).fail(function() {
		console.log("Unable to get token");
	});	
}//uploadPhotoToAlbum	

function sendFileToAlbum(){	
	var uploadToken = window.localStorage.getItem("satToken");
	var context_id = window.localStorage.getItem("satUserID");
	var imageLocation = jQuery("#file-to-upload-album-image").val();
	var imageData = window.localStorage.getItem("satAlbumImageObject");
	var album_id = jQuery("#load-more-album-photos").attr('album-id');
	
	var imageTitle = jQuery("#album_photo_title").val();
	if(imageTitle=='') {
		imageTitle = imageLocation;
	}
	console.log(imageData);	
	var uploadData = {
		action 			: "rtmedia_api",
		method			: "rtmedia_upload_media",
		token 			: uploadToken,
		rtmedia_file 	: imageData,
		context 		: "profile",
		context_id		: context_id,
		album_id		: album_id,
		media_type		: "photo",
		title 			: imageTitle,
		image_type		: "jpeg"
	};
	console.log("uploadData:");
	console.log(uploadData);
	console.log("----------------");
	jQuery.post('http://www.shareatalent.com/wp-admin/admin-ajax.php',uploadData,function(u){
			console.log(u);
			JSON.stringify(u);
			var status = u.status;		
			//if(status == 'TRUE'){alert("Got it!")} else {alert("Nope!")}
			jQuery.mobile.loading('hide');
			// from cancel
			jQuery("#make-a-post").slideUp("fast");
			jQuery("#cancel-post-update-container").css("display", "none");
			jQuery("#post-update-button-container").css("display", "block");
			window.localStorage.setItem("SATempImage", null);
			imageData = '';
			var largeImage = document.getElementById('largeImage');
			largeImage.style.display = 'none';
			largeImage.src = imageData;
			smallImage.style.display = 'none';
			smallImage.src = imageData;
			jQuery("#post-update-content").val('');
			jQuery("#post-update-messages").html('<h3 style="color:red;">Posted!</h3>');
			jQuery("#post-update-messages").slideDown("fast");
			jQuery("#post-update-messages").delay(2000).slideUp("fast");
		},'json');
}// sendFileToAlbum
	