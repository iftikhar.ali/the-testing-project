var image_changed = false;


function takePic(){
	event.preventDefault();
	if (!navigator.camera) {
	  alert("Camera API not supported", "Error");
	  return;
	}
	var options =   {   quality: 50,
	  destinationType: Camera.DestinationType.FILE_URI,
	  sourceType: 1,      // 0:Photo Library, 1=Camera, 2=Saved Album
	  encodingType: 0     // 0=JPG 1=PNG
	};

	navigator.camera.getPicture(
	function(imgData) {
        jQuery(".avatar").attr('src',imgData).css('width','91px').css('height','91px');
		image_changed = true;
        if(image_changed) {
            console.log("Ajax function");
            uploadPhoto(imgData);
        }
	},
	function() {
	  //Ignore
	},
	options);

	return false;
}
	
                
function choosePic(){
   event.preventDefault();
	if (!navigator.camera) {
	  alert("Camera API not supported", "Error");
	  return;
	}
	var options =   {   quality: 50,
	  destinationType: Camera.DestinationType.FILE_URI,
	  sourceType: 0,      // 0:Photo Library, 1=Camera, 2=Saved Album
	  encodingType: 0     // 0=JPG 1=PNG
	};

	navigator.camera.getPicture(
	function(imgData) {
        jQuery(".avatar").attr('src',imgData).css('width','91px').css('height','91px');
		image_changed = true;
        if(image_changed) {
            console.log("Ajax function");
            uploadPhoto(imgData);
        }
	},
	function() {
	  //Ignore
	},
	options);

	return false;
}

function uploadPhoto(imageURI) {
    
    var UserID         =   window.localStorage.getItem("satUserID");
	var options        =   new FileUploadOptions();
	options.fileKey    =   "file";
	options.fileName   =   UserID+"-bpfull.jpg";
	options.mimeType   =   "image/jpeg";    
    var serverUrl      =   "http://www.shareatalent.com/api/upload_photo.php";  
    
    var params = {};
    params.value1 = UserID;
    params.value2 = imageURI;
    
	var params = {};
	options.params = params;
	var fileTransfer = new FileTransfer();
    fileTransfer.upload(imageURI, encodeURI(serverUrl), onUploadSuccess, onUploadFail, options, true);
   
}

function onUploadSuccess(r) {
    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
}

function onUploadFail(error) {
    alert("An error has occurred: Code = " + error.code);
    console.log("upload error source " + error.source);
    console.log("upload error target " + error.target);
}


function onConfirm(buttonIndex) {
    if(buttonIndex > 0) {
        if(buttonIndex == 1) {
            choosePic();
        } else if(buttonIndex == 2) {
            takePic();
        }
    }
}

function changeProfileImage() {    
    navigator.notification.confirm(
        'Do you want to change, your profile picture ?. Touch elsewhere to dismiss.', // message
         onConfirm,                     // callback to invoke with index of button pressed
        'Change the profile image ?',   // title
        ['Choose pic.','Take pic.']     // buttonLabels
    );
}