jQuery.noConflict();

document.addEventListener('deviceready', TalentsFunctions, false);

// Primary Level
function TalentsFunctions() {
cl("Talents Functions ready!");
var sec;
var nam;
var second;
var name;
jQuery(document).ready(function() 
{	
	"use strict";
	
	jQuery("a[id^='my-talents-menu']").on("vclick",function()
		{	
			cl("clicked settings");
			var UserID = window.localStorage.getItem("satUserID");
			cl("UserID: "+UserID);
			var topLevel = jQuery("#my-talent-select").val();
			cl("Top Level: "+topLevel);						
			var talentURL = 'http://www.shareatalent.com/api/talent/talent_top_level_api/?userid='+UserID+'&level=top';	
			//cl(talentURL);
		jQuery.getJSON(talentURL, 
			function(talent){
			//cl(talent.top);
			var top = talent.top.top;
			var sec = talent.top.sec;
			var nam = talent.top.nam;
			//jQuery("#my-talent-category").text(top);
			//jQuery("#my-talent-category").val(top);
			var option 	= '<option value="'+top+'" selected id="top_level_6">'+top+'</option>';
			var second 	= '<option value="'+sec+'" selected >'+sec+'</option>';
			var name	= '<option value="'+nam+'" selected >'+nam+'</option>';	
				
				jQuery("#talent-category").append(option)
				jQuery("#talent-category").trigger("change");
				jQuery("#talent-category").selectmenu("refresh");
				
				jQuery("#talent-type").append(second);
				jQuery("#talent-type").trigger("create");
				jQuery("#talent-type").selectmenu("refresh");
				
				jQuery("#talent-type-div").css("display","block");
				
				jQuery("#talent-name").append(name);
				jQuery("#talent-name").trigger("create");
				jQuery("#talent-name").selectmenu("refresh");
				
				jQuery("#talent-name-div").css("display","block");
				SelectTalentsFunctions();
			});								
		});	

// Top Level Talent

function SelectTalentsFunctions(){
	cl("TalentReady");
	jQuery("#talent-category").on("change",function()
			{
				var UserID = window.localStorage.getItem("satUserID");
                var top_selected = encodeURIComponent(jQuery("#talent-category").val());
				//cl(top_selected);
				jQuery("#talent-name-div").css("display","none");
				//jQuery("#talent-name-div").css("display","none");		
                var talentCatURL = 'http://www.shareatalent.com/api/talent/talent_second_level/?userid='+UserID+'&top='+top_selected;
				cl("talentURL: "+talentCatURL);
				jQuery.getJSON(talentCatURL,
				function(talentCats){
						var talentCatsStr = JSON.stringify(talentCats);
						cl("talentCatsStr: "+talentCatsStr);
						cl("talentCats: "+talentCats.second.category);
							jQuery("#talent-type").empty();							
						jQuery.each(talentCats.second, function (result, value){	
							jQuery("#talent-type").append(
							'<option value="'+value.category+'">'+value.category+'</option>');
							jQuery("#talent-type").trigger("create");		
						});
						jQuery("#talent-type").append('<option value="" selected >Select a Category</option>');
						jQuery("#talent-type").trigger("create");
						jQuery("#talent-type").selectmenu("refresh");
					});
		});
	
/****************************/
/***** 2nd Level Talent *****/
/****************************/
	jQuery("#talent-type").on("change",function()
		{
			var UserID = window.localStorage.getItem("satUserID");
			var cat_selected = encodeURIComponent(jQuery("#talent-type").val());
			cl(cat_selected);				
			var talentNameURL = 'http://www.shareatalent.com/api/talent/talent_name_level/?userid='+UserID+'&cat='+cat_selected;
			//cl("talentURL: "+talentNameURL);
		
			jQuery.getJSON(talentNameURL,
			function(talentNames){
					var talentNamesStr = JSON.stringify(talentNames);
					cl("talentNamesStr: "+talentNamesStr);
					cl("talentNames: "+talentNames);
						jQuery("#talent-name").empty();							
					jQuery.each(talentNames.name, function (result, value){	
						jQuery("#talent-name").append('<option value="'+value.id+'">'+value.name+'</option>');
						jQuery("#talent-type").trigger("create");
						jQuery("#talent-name-div").css("display","block");
					});					
				});
		
	});
	
	jQuery("#talent-name").on("vclick",function()
			{
				jQuery("#my-talent-update-button").css("display","block");				
			});
/***********************/
/***** Save Talent *****/
/***********************/
			
	jQuery("#my-talent-update-button").on("vclick",function()
			{
				var UserID = window.localStorage.getItem("satUserID");
				var talent_id 	= jQuery("#talent-name :selected").val();
				var talent		= jQuery("#my-talent-select :selected").val();
				cl("tid: "+talent_id+" User: "+UserID+" Talent #: "+talent);
				 var SaveTalentURL = 'http://www.shareatalent.com/wp-content/themes/bp-talent-sharing/custom-functions/talent/mobile/mob_save_talent.php?userid='+UserID+'&tid='+talent_id+'&talent='+talent;
				cl(SaveTalentURL);
				jQuery.ajax({
					url: SaveTalentURL,
					success: function(){
					jQuery("#save-talent-message").slideDown( 500 ).html('<h1 style="color:red;">Talent Saved!</h1>');
					jQuery("#save-talent-message").delay( 2000 ).slideUp( 500 );
					}
					
					});
			});
			
}// end SelectTalentFunctions
	});

}