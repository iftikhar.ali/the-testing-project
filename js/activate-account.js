jQuery(document).ready(activate);

function activate(){

var satUserID 		= localStorage.getItem("satUserID");
var hash 			= localStorage.getItem("sat_hash");
var sat_username 	= localStorage.getItem("sat_username");
var sat_email 		= localStorage.getItem("sat_email");
var sat_type 		= localStorage.getItem("sat_type");	

console.log("user type: "+sat_type);

if(sat_type == null){sat_type_null()} 
if(sat_type == 'undefined'){sat_type_undefined()} 
if(sat_type == 0){sat_type_0()} 
if(sat_type == 1){sat_type_1();}
if(sat_type == 2){sat_type_2()}
if(sat_type == 3){sat_type_3()}

function sat_type_undefined(){
	jQuery("#activate-type-undefined").slideDown("fast");	
	}
	
function sat_type_null(){
	jQuery("#activate-type-null").slideDown("fast");	
	}

function sat_type_0(){
	jQuery("#activate-type-0").slideDown("fast");	
	}

function sat_type_2(){
	jQuery("#activate-type-2").slideDown("fast");	
	}

jQuery("#send-sms-activation").on("click",function(){
	jQuery("#activation-code-message").html('Checking...');
	var code = jQuery("#activation-code").val().toUpperCase();
	userID = window.localStorage.getItem("satUserID");
	activateURL = 'http://www.shareatalent.com/api/smsActivate.php?user='+userID+'&code='+code;
	console.log(activateURL);
	jQuery.getJSON(activateURL,function(data){
		console.log(data.status);
		console.log(data.message);
		if (data.status == 'ok'){
			console.log(data);
			window.localStorage.setItem("sat_type",1);
			var redirectURL = 'index.html';
			window.location = redirectURL;
			}
		else if (data.status == 'error'){
			jQuery("#activation-code-message").html(data.message);
			jQuery("#send-sms-activation").text("Try Again!");
			}
		}) // getJSON
	});// click activate_button

jQuery("#resend_activate_button").on("click",function(){
	jQuery("#resendSMSMessage").html('Sending...');
	userID = window.localStorage.getItem("satUserID");	
	resendActivateURL = 'http://www.shareatalent.com/api/smsResendActivate.php?user='+userID;
	console.log(resendActivateURL);
	jQuery.getJSON(resendActivateURL,function(data){
		console.log(data.status);
		console.log(data.message);
		console.log(data.debug);
		jQuery("#resendSMSMessage").html('<p style="color:red;">'+data.message+'</p>');
		jQuery("#resend_activate_button").html('Resend to my phone... again?!');
		}) // getJSON
	});// click resend_activate_button

jQuery("#active_by_email").on("click",function(){
	jQuery("#activate-type-0").toggle();
	jQuery("#activate-type-2").toggle();
	});// active_by_email

/*
jQuery("#activate_via_sms").on("click",function(){
	jQuery("#activate-type-2").toggle();
	jQuery("#activate-type-0").toggle();
	});// active_by_email
*/
	
jQuery("#activate_button").on("click",function(){
					
		var userID =  window.localStorage.getItem("satUserID");	
		var code = jQuery.trim(jQuery("#activation_code").val().toUpperCase());
		var sec = jQuery.trim(jQuery("#security_answer").val());
		console.log("nuid: "+userID+" code: "+code+" sec: "+sec);
		
		var activate_url = 'http://www.shareatalent.com/api/createuser/verifyAccount/?uid='+userID+'&act='+code+'&sec='+sec+'&callback=?';
		console.log(activate_url);	
		jQuery.getJSON(activate_url,function(act){
			console.log(act);
			if(act.activated == 1){
				window.localStorage.setItem("sat_type",3);
				window.location = ("index.html");
				}
			else {
				console.log("Error!")
				jQuery("#activation_message").html(act.message);
				jQuery("#activation_message").slideDown("fast");
				jQuery("#activation_message").css("color","red");
				}
			})
			.done(function(){
				jQuery("#activate_button").html('<span>Activate</span>');
		jQuery("#activate_button").attr("disabled",false);
				}) // end getJSON		
		})// end on click
		
jQuery("#resend_activate_button_email").on("click",function(){	
	jQuery("#resend-message").html('Sending...');
	jQuery("#resend-message").slideDown("fast");	
	
	var userID =  window.localStorage.getItem("satUserID");	
	var resend_url = 'http://www.shareatalent.com/api/emailResendActivate.php?user='+userID;
	console.log(resend_url)
	
	jQuery.getJSON(resend_url,function(res){
		console.log(res)
		jQuery("#resend-message").html('<p style="color:red;">'+res.message+'</p>');
		}).done(function(){			
			jQuery("#resend-message").slideDown("fast");			
			});
	})// end resendActivation click


}// activate