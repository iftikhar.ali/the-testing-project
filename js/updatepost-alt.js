var pictureSource;   // picture source
    var destinationType; // sets the format of returned value

    // Wait for device API libraries to load
    //
    document.addEventListener("deviceready",altPostUpdate,false);
	
    // device APIs are available
    //
    function altPostUpdate() {
		cl("Ready for Uploading! (updatepost-alt.js)");
        pictureSource=navigator.camera.PictureSourceType;
        destinationType=navigator.camera.DestinationType;
    }

    // Called when a photo is successfully retrieved
    //
    function onPhotoDataSuccess(imageData) {
      // Uncomment to view the base64-encoded image data
      // console.log(imageData);
	cl("onPhotoDataSuccess(imageData) called");
	window.localStorage.setItem("SATempImage", null);
	window.localStorage.setItem("SATempImage", imageData);
	var smallImage = document.getElementById('smallImage');

	smallImage.style.display = 'block';
	smallImage.src = "data:image/jpeg;base64," + imageData;
	
}

function onPhotoURISuccess(imageURI) {
	cl("onPhotoURISuccess(imageURI) called");
	window.localStorage.setItem("SATempImage", null);
	window.localStorage.setItem("SATempImage", imageURI);
  
	var largeImage = document.getElementById('largeImage');
	largeImage.style.display = 'block';
	largeImage.src = imageURI;

}

    function capturePhoto() {
	  // Take picture using device camera and retrieve image as base64-encoded string
      cl("capturePhoto triggered");
	  var allMediaType = navigator.camera.MediaType.ALLMEDIA;
	  navigator.camera.getPicture(onPhotoDataSuccess, onFail, { 
	  quality: 50,
      destinationType: destinationType.DATA_URL, 
	  mediaType: allMediaType 
	  });
    }

    function getPhoto(source) {	
      // Retrieve image file location from specified source
	  cl("getPhoto triggered");
      var mediaType = navigator.camera.MediaType.ALLMEDIA;
	  navigator.camera.getPicture(onPhotoURISuccess, onFail, { 
	  quality: 50,
      destinationType: destinationType.FILE_URI,
      sourceType: source,
	  mediaType: mediaType 
	  });
    }

    function onFail(message) {
      alert('Failed because: ' + message);
    }