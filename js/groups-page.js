document.addEventListener('deviceready', getGroupsPageFunction, false);

function getGroupsPageFunction(){
	cl("Groups Page Functions Ready!");
jQuery("a[id^='groups-page-menu'], #main-profile-groups-link").on("vclick",function() 
{
jQuery("#groups-content-list").empty();
var UserID = window.localStorage.getItem("satUserID");	
var UserName = window.localStorage.getItem("satUserName");	

cl("UserID: "+UserID);
cl("UserName: "+UserName);
	if (UserID>0) 
	{		
		var GroupsURL = 'http://www.shareatalent.com/api/buddypressread/groups_get_groups/?username='+UserName;
			cl("GroupsURL: "+GroupsURL);
			jQuery.getJSON(GroupsURL,
			function(GroupsList){
					var status = GroupsList.status;
					if (status == 'error'){jQuery("#groups-content-list").html('<li>No groups found.</li>').trigger("create").listview("refresh");}
					else {
					cl("GroupsList: "+GroupsList.groups);
					jQuery.each(GroupsList.groups, function (result, value){	
						jQuery("#groups-content-list").append(
						//'<option value="'+value.category+'">'+value.category+'</option>');
						'<li><a href="#groups-content-page" class="group-details" group-id="'+result+'">'+value.name+'<meta value="'+result+'" /></a></li>');
						jQuery("#groups-content-list").trigger("create").listview("refresh");		
					}); // end each
					}// end else
				});
	
	}// end if actual user
	}); // end function
jQuery.mobile.loading('hide');

} // end function
jQuery("#groups-content-list").on("click",".group-details", function(){
	jQuery("#list-of-my-group-recent-activity").html('');
	//alert(jQuery(this).attr('group-id'));
	var group_id = jQuery(this).attr('group-id');
	var UserID = window.localStorage.getItem("satUserID");
jQuery.mobile.loading('show');
var pageNumber =1;
var getActivitiesURL = 'http://www.shareatalent.com/api/get_group_activities.php?group_id='+group_id;
jQuery.getJSON(getActivitiesURL,function(d){
	var string=JSON.stringify(d);
	//alert(string);
	if(d.group_activities.length > 0){
		var n=0;
		jQuery("#list-of-my-group-recent-activity").empty();
		jQuery.each(d.group_activities,function(results,values){
			console.log(values);
			var todaysDate = new Date(values.date_recorded);
			var diff = Math.abs(new Date() - todaysDate);
			var ms = diff;
			var hour = Math.floor((ms/1000/60/60));
			var min = Math.floor((ms/1000/60)%60);
			var sec = Math.floor((ms/1000) % 60);
			
			var timeString = ((hour)?(' '+hour+' hours '):'')+((min)?(' '+min+' minutes ago'):'');
			if(timeString==='') {timeString = ' '+sec+' seconds ago';}
			var date = new Date();
			if((date.getDate()-1) == todaysDate.getDate()) {
				timeString = "Yesterday";
			}
			if((date.getDate()-todaysDate.getDate())>1) {
				timeString = ' on '+todaysDate.getDate()+'-'+todaysDate.getMonth()+'-'+todaysDate.getFullYear();
			}
			var appendContents = '<div class="activity-content">'+
		'<div class="activity-header">'+
			'<p><a href="http://www.shareatalent.com/members/'+values.user_login+'/" title="'+values.user_login+'">'+values.action+'<span class="time-since">'+timeString+' ago</span></a></p>'+
		'</div>'+
			'<div class="activity-inner">'+
				'<p>'+values.content+'</p>'+
			'</div>'+
	'</div>	'+
'</li>';
			
			jQuery("#list-of-my-group-recent-activity").append(appendContents);
			jQuery("#list-of-my-group-recent-activity").trigger("create").listview("refresh");
			
			});// each
		}// if status ok
	if(d.status == 'error'){
		var n=0;		
			console.log(d.message);
			jQuery("#list-of-my-group-recent-activity").append('<li style="list-style:none; font-size:14px; font-weight:normal; border-top:1px solid #ccc;" class="ui-li ui-li-static">'+d.message+'</li>');
			jQuery("#list-of-my-group-recent-activity").trigger("create").listview("refresh");
			n++;			
		}// if status
	jQuery("#load-more-group-activities").attr('current-page-number',(parseInt(pageNumber)+1));
	});// getJSON
	jQuery.mobile.loading('hide');
	
});

/*
jQuery("#load-more-group-activities").on("vclick",function(event){
	var group_id = jQuery("#load-more-group-activities").attr('group-id');
jQuery.mobile.loading('show');
var pageNumber = jQuery("#load-more-group-activities").attr('current-page-number');
var UserID = window.localStorage.getItem("satUserID");
	var getActivitiesURL = 'http://www.shareatalent.com/api/get_group_activities.php?group_id='+group_id;
jQuery.getJSON(getActivitiesURL,function(d){
	if(d.group_activities.length > 0){
	jQuery("#load-more-group-activities").attr('current-page-number',(parseInt(pageNumber)+1));
		jQuery.each(d.group_activities,function(results,values){
			console.log(values);
			var todaysDate = new Date(values.date_recorded);
			var diff = Math.abs(new Date() - todaysDate);
			var ms = diff;
			var hour = Math.floor((ms/1000/60/60));
			var min = Math.floor((ms/1000/60)%60);
			var sec = Math.floor((ms/1000) % 60);
			
			var timeString = ((hour)?(' '+hour+' hours '):'')+((min)?(' '+min+' minutes '):'');
			if(timeString==='') {timeString = ' '+sec+' seconds';}
			var appendContents = '<div class="activity-content">'+
		'<div class="activity-header">'+
			'<p><a href="http://www.shareatalent.com/members/'+values.user_login+'/" title="'+values.user_login+'">'+values.action+'<span class="time-since">'+timeString+' ago</span></a></p>'+
		'</div>'+
			'<div class="activity-inner">'+
				'<p>'+values.content+'</p>'+
			'</div>'+
	'</div>	'+
'</li>';
			
			jQuery("#list-of-my-group-recent-activity").append(appendContents);
			jQuery("#list-of-my-group-recent-activity").trigger("create").listview("refresh");
			
			})// each
		}// if status ok
	if(d.status == 'error'){
		var n=0;		
			console.log(d.message);
			jQuery("#list-of-my-group-recent-activity").append('<li style="list-style:none; font-size:14px; font-weight:normal; border-top:1px solid #ccc;" class="ui-li ui-li-static">'+d.message+'</li>');
			jQuery("#list-of-my-group-recent-activity").trigger("create").listview("refresh");
			n++;			
		}// if status
	});// getJSON
jQuery.mobile.loading('hide');
});*/