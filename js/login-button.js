document.addEventListener('deviceready', LoginFunction, false);
function LoginFunction() { 
console.log("Ready for Login!");
var versioning = "1.0.47";
jQuery("#versioning").html("<small>"+versioning+"</small>");
jQuery("#versioning-sidebar").html("<small>"+versioning+"</small>");
			   console.log("version: "+versioning);

  jQuery.mobile.allowCrossDomainPages = true;
  jQuery.support.cors = true;

console.log('Checking Login');
var UserID = "";
var Parent = "";
var UserType = "";
var authData2;
var nonce = '';
/**** Check to see if user already logged in ****/
	jQuery("#top-nav").css("display", "none");
	
	jQuery('#user').on("focusin", function(){
		console.log("user field adjusted for android")
		jQuery("#login-form").css("height", "1200px");
		jQuery('html, body').animate({
			scrollTop: jQuery('#user').offset().top - 20		
			});
	});
	jQuery('#user').on("focusout", function(){
		jQuery("#login-form").css("height", "100%");
		});
	jQuery('#pass').on("focusin", function(){
		console.log("pass field adjusted for android")
		jQuery("#login-form").css("height", "1200px");
		jQuery('html, body').animate({
			scrollTop: jQuery('#pass').offset().top - 20		
			});
	});
	jQuery('#pass').on("focusout", function(){
		jQuery("#login-form").css("height", "100%");
		});
	var UserID = window.localStorage.getItem("satUserID");	
	var UserType = window.localStorage.getItem("sat_type");
	console.log(UserType);
	if (UserID>0 ) {	
	if(UserType == 0 || UserType == 2 || UserType == 9 || UserType == null || UserType == 'undefined'){		
		window.location.assign("activate.html");
		}
		/*
		jQuery("#content").css("display","block");		
		jQuery("#login-form").css("display", "none");
		jQuery("#login-form").css("display", "none");		
		jQuery("#user-id div").text(UserID);
		*/
		console.log('Already logged in as User#: '+UserID);	
		//display member's page
			var userURL = 'http://www.shareatalent.com/api/userprofile/userProfile_api/?userid='+UserID+'&callback=?';	
				jQuery.getJSON(userURL,
				function(userProfile){ 
					var userProf = JSON.stringify(userProfile);
					//console.log('Profile: ' + userProf);
					//jQuery("#profile").html(userProfile.html);
					
					/* Profile Header Content */
					jQuery("#gravatar").html(userProfile.user.gravatar);
					jQuery("#displayname").html(userProfile.user.displayname);
					jQuery("#talent_1_top").html(userProfile.user.talent_1_top);
					jQuery("#talent_1_lv2").html(userProfile.user.talent_1_lv2);
					jQuery("#talent_1_name").html(userProfile.user.talent_1_name);
					
					/* Profile Options Content */
					jQuery("#endoresment_count").html(userProfile.user.endoresment_count);
					//jQuery("#activity_count").html(userProfile.user.activity_count);		
					jQuery("#friend_count").html(userProfile.user.friend_count);
					jQuery("#friend_request_count").html(userProfile.user.friend_request_count);
					
					/* About Talent Content */
					jQuery("#talent_1_about").html(userProfile.user.talent_1_about);									
					
						
					window.location.assign("index.html#main-profile");
					console.log("Redirect to #main-profile");
					jQuery("#top-nav").css("display", "block");
					
					/* Check for IsParent  */
					var Parent = userProfile.user.parent;
					console.log("Parent: " + userProfile.user.parent);
					window.localStorage.setItem("IsParent", Parent);
					var Parent = window.localStorage.getItem("IsParent");
					
					if (Parent == null || Parent == "No") {
						var Parent = "No";
						jQuery("#child-accounts-menu").css("display", "none");	
						jQuery("#user-settings-menu").listview("refresh");
						console.log("Refreshed ");
						}
					
					else if(Parent == "Yes") {
						jQuery("child-accounts-menu").css("display", "block");							
						jQuery("#user-settings-menu").listview("refresh");
						console.log("Refreshed ");
					}
					
				});		
		} 
/**************************************************/		
/************* Not logged in **********************/		
/**************************************************/		
		else {
	console.log('Not Logged In');
	window.localStorage.clear();
	var login_status = '';	
	var the_status = '';
	var the_message = '';
	var UserType = '';
	jQuery("#login-button").css("display","block");
	jQuery("#login-submit-button").on("click",function(event)	
		{	
			console.log('login-button clicked');
			event.preventDefault();
			console.log('preventDefault');
			jQuery.mobile.loading('show');
			jQuery("#create-account-messages").empty();
			
			var user = 	jQuery("#user").val();
			var pass = 	jQuery("#pass").val();	
			window.localStorage.setItem("satpass", pass);
			window.localStorage.setItem("satUserName", user);
			var cb 	 =  '&callback=?';	
			var nonceURL =  'http://www.shareatalent.com/api/get_nonce/?controller=auth&method=generate_auth_cookie'+cb;						
			console.log('nonceURL: '+nonceURL);
			console.log('callback: '+cb);
		// Grab a nonce, any nonce'll do
		jQuery.getJSON(nonceURL)
			.success(function(json){
				var nonce = json.nonce;
				jQuery("#user-nonce").text(nonce);					
				console.log('Nonce: '+ nonce);				
				logMeInPlease(nonce);			
				})
			.fail(console.log("Trying my best..."));
		
function logMeInPlease(nonce){
	var authURL = 'http://www.shareatalent.com/api/auth/generate_auth_cookie/';	
	var authFull = 'http://www.shareatalent.com/api/auth/generate_auth_cookie/?username='+user+'&password='+pass+'&nonce='+nonce+'&callback=?';			
		console.log('authFull: '+ authFull);
		console.log('getJSON');	
		jQuery.getJSON(authFull,function(d){			
			console.log("Response from auth:")
			console.log(d)
			if(d.status == "error"){
				var str = d.error;
				var n = str.search(' var in your');
				console.log(n);
				console.log(d.error);
								
				if(n > 0){ 
				console.log('Empty user/pass');
				jQuery.mobile.loading('hide');
				jQuery("#login-messages").html('<p style="color:red;">You missed something! Make sure you have a username and password entered.</p>');
				} else if(n < 0) {
				console.log('Bad username');
				jQuery.mobile.loading('hide');
				// TEST to see if account is locked
				
				jQuery("#login-messages").html('<p style="color:red;">'+d.error+'</p>');
				}
				
				jQuery("#login-submit-button").parent('div').removeClass('ui-btn-active');
				console.log("removeClass");
			}//if error
			else {
				console.log(d.user);
				jQuery.mobile.loading('hide');
				jQuery("#login-messages").html('<p>Login Successful! Gimme a sec...</p>');
				successfulNonce(d);
			}// else
			});
	}// logMeInPlease				

	});// end AJAX line 109		
	}// end else

}// end loginFunction
	
function successfulNonce(d) {
	console.log(d)
	var the_status = d.status;
	console.log('STATUS: '+ the_status);	
	// error status
	if (the_status == 'error') {
		console.log("CRAP!! Line 133 "+login_status.error);
		jQuery.mobile.loading('hide');
		}	

// IT WORKED!!
if(the_status == 'ok') {
console.log("Setup Token")
var passToken = window.localStorage.getItem("satpass");
var passUserName = window.localStorage.getItem("satUserName");
var tokenURL = 'http://www.shareatalent.com/wp-admin/admin-ajax.php';
var tokenData = {
	action		: "rtmedia_api",
	method		: "wp_login",
	username  	: passUserName,
	password  	: passToken
	};
jQuery.post(tokenURL,tokenData,function(t){
	console.log("Token created")
	console.log(t);
    tp = JSON.parse(t);
	console.log("Parsed Token:"+tp.data.access_token);
    token = tp.data.access_token;
	window.localStorage.setItem("satToken",token);
	console.log("SAtToken: ");
	console.log(tp.data.access_token)
	console.log("--------------");
	keepLogging(d);
	}).fail(function() {
    console.log("Unable to get token");
  });	
function keepLogging(){
console.log("ID: "+d.user.id);
console.log("sat_type: "+d.user.type);
	var UserID 			= d.user.id;
	var UserName 		= d.user.username;
	var UserDisplayName = d.user.displayname;
	var UserEmail 		= d.user.email;
	var satType 		= d.user.type;
	console.log("Check UserID: "+UserID);
	window.localStorage.setItem("satUserID", UserID);
	window.localStorage.setItem("satUserName", UserName);
	window.localStorage.setItem("satUserDisplayName", UserDisplayName);
	window.localStorage.setItem("satUserEmail", UserEmail);
	window.localStorage.setItem("sat_type", satType);

var userURL = 'http://www.shareatalent.com/api/userprofile/userProfile_api/?userid='+UserID+'&callback=?';	
			jQuery.getJSON(userURL,
			function(userProfile){ 			
			var userProf = JSON.stringify(userProfile);

			var Parent = userProfile.user.parent;
			window.localStorage.setItem("IsParent", Parent);
			console.log("Check Parent: "+Parent);												

			window.location.assign("index.html#main-profile");						
			jQuery("#top-nav").css("display", "block");	
			//if(Parent = "Yes") {jQuery(".child-account-settings-menu").css("visibility", "visible");	}						
			jQuery.mobile.changePage("index.html#main-profile");
			location.reload();
			console.log("Page Reloaded");
			}); //end userProfile

	}	// end if ok
}// end keepLogging

}// successfulNonce

