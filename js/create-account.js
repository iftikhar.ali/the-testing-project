jQuery(window).load(createAccountCredentials); 
jQuery(window).load(yearTest); 
jQuery(window).load(passTest); 
jQuery(window).load(phoneCheck); 
jQuery(window).load(postalCheck); 
jQuery(window).load(phoneValidate); 
jQuery(window).load(tosCheck); 
jQuery(window).load(noPhone); 

function clearAccountSessionInfo(){
	console.log("Reset Account Creation data");
	window.localStorage.removeItem("passwordPassed");
	window.localStorage.removeItem("postalPassed");
	window.localStorage.removeItem("phonePassed");
	window.localStorage.removeItem("noPhone");
	window.localStorage.removeItem("badPass");
	}

function yearTest(){
	console.log("Ready to test the user's year!")
	jQuery('#year-test').on("change",function(){
		var yearOfBirth = jQuery('#year-test option:selected').val();
		console.log("Year test changed!! "+yearOfBirth);
		var d = new Date();
		var thisYear = d.getFullYear();
		var age = thisYear - yearOfBirth;
		console.log("Your age: "+age);
		if (age <= 13) {
			jQuery("#parents-permission-needed").slideDown("fast");
			jQuery("#check-user-email").slideUp("fast");
			}
		if (age >= 14) {
			console.log("Age Passed!")
			jQuery("#check-user-email").slideDown("fast");
			jQuery("#parents-permission-needed").slideUp("fast");
			console.log("scroll to check-user-email")
			jQuery('html, body').animate({scrollTop: jQuery('#check-user-email').offset().top - 20});		
			}
		})
	}; // end yearTest

function passTest(){
jQuery('#password').on("click", function(){
		jQuery('html, body').animate({
			scrollTop: jQuery('#password').offset().top - 20		
			});
	});
jQuery('#passwordChange').on("click", function(){
		jQuery('html, body').animate({
			scrollTop: jQuery('#password').offset().top - 20		
			});
	});
	
jQuery("#password").on("change",function(){			
	var passwordStrengthRegex = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,15})/gm;
		var password = jQuery("#password").val();
		passStrength = password.match(passwordStrengthRegex);
		console.log('passwordStrengthRegex: '+passStrength);
		if(passStrength == null) {
			localStorage.setItem("AccountCreationError", 1);
			jQuery("#passwordMessage").html('<p style="color:red;">Password MUST include uppercase letter, lowercase letter, number, and be 6-15 characters.</p>');
			jQuery('html, body').animate({
			scrollTop: jQuery('#show-form').offset().top - 20		
			});
			window.localStorage.setItem("badPass",false);
			jQuery("#password").val('');
			jQuery("#password").focus();
			}
		else {jQuery("#passwordMessage").html('');window.localStorage.setItem("badPass",true);}
});

	jQuery("#passwordConfirm").on("change",function(){		
		console.log("password: "+jQuery("#password").val());
		var password = jQuery("#password").val();
		var passConf = jQuery("#passwordConfirm").val();
		console.log("password: "+password);
		console.log("passConf: "+passConf);
		if(password == passConf){
			jQuery("#passwordMessage").html('<span style="color:darkgreen;">Passwords Match!</span>');
			console.log("password matched!");
			localStorage.removeItem("AccountCreationError");
			localStorage.setItem("passwordPassed", true);
			} else {
			jQuery("#passwordMessage").html('<span style="color:red;">Passwords Do Not Match!</span>');
			console.log("password mismatched!");
			localStorage.setItem("AccountCreationError", 1);
			jQuery("#passwordConfirm").val('');
			jQuery("#passwordConfirm").focus();
			jQuery('html, body').animate({
			scrollTop: jQuery('#show-form').offset().top - 20		
			});
			}		
		});
	}	// end passTest

function phoneCheck(){
	jQuery('#phone').on("click", function(){
		jQuery('html, body').animate({
			scrollTop: jQuery('#phoneCountry').offset().top - 20		
			});
	});
	
	jQuery('#phoneCountry').on("change", function(){		
		jQuery('#phone').val('');
		var phoneVal = jQuery('#phoneCountry :selected').val();
		console.log(phoneVal);
		if(phoneVal == 1){
			jQuery('#phone').attr("placeholder","###-###-####");
			jQuery('#phone').removeAttr("disabled");
			}
		if(phoneVal == 91){
			jQuery('#phone').attr("placeholder","AAAA-BBBB");
			jQuery('#phone').removeAttr("disabled");
			}
		if(phoneVal == 49){
			jQuery('#phone').attr("placeholder","AAAA BBBBBB");
			jQuery('#phone').removeAttr("disabled");
			}
		if(phoneVal == 0){
			jQuery('#phone').attr("placeholder","");
			jQuery('#phone').attr("disabled","disabled");
			jQuery('#phone').val('');
			}
		})// end on change
	}	// end phoneCheck

function postalCheck(){
	jQuery('#postal').on("click", function(){
		jQuery('html, body').animate({
			scrollTop: jQuery('#postal').offset().top - 20		
			});
	});

		jQuery("#countrySelect").on("change",function(){
		jQuery("#postal").val('');
		jQuery("#postalMessage").html('');
		localStorage.removeItem("postalPassed");
		var country = jQuery("#countrySelect :selected").val();
		console.log("Country: "+country);
		if(country !== "blank"){
			jQuery('#postal').removeAttr("disabled");
			if(country == "United States"){
				jQuery('#postal').attr("placeholder","#####");
				}
			if(country == "Germany"){
				jQuery('#postal').attr("placeholder","####");
				}
			if(country == "India"){
				jQuery('#postal').attr("placeholder","######");
				}	
			} // end if country not blank
		});// end countrySelect change
	/*
	jQuery('#postal').on("change", function(){
		jQuery('html, body').animate({
			scrollTop: jQuery('#phoneCountry').offset().top - 20		
			});
	});
	*/
	jQuery('#postal').on("change", function(){
		var postalCode = jQuery('#postal').val().replace(/\D/g,'');
		var postLen	= postalCode.length;
		console.log("postal code: "+postalCode);		
		var country = jQuery("#countrySelect :selected").val();		
			if(country == "United States" && postLen !== 5){
				jQuery('#postalMessage').html('<span style="color:red;">That postal code is not right!</span>');
				localStorage.removeItem("postalPassed");
				console.log("postalPassed removed");
				}
			if(country == "United States" && postLen == 5){
				jQuery('#postalMessage').html('<span style="color:darkgreen;">That postal code looks good!</span>');
				localStorage.setItem("postalPassed", true);
				}
			if(country == "Germany" && postLen !== 4){
				jQuery('#postalMessage').html('<span style="color:red;">That postal code is not right!</span>');
				localStorage.removeItem("postalPassed");
				console.log("postalPassed removed");
				}
			if(country == "Germany" && postLen == 4){
				jQuery('#postalMessage').html('<p style="color:darkgreen;">That postal code looks good!</p>');
				localStorage.setItem("postalPassed", true);
				}
			if(country == "India" && postLen !== 6){
				jQuery('#postalMessage').html('<span style="color:red;">That postal code is not right!</span>');
				localStorage.removeItem("postalPassed");
				console.log("postalPassed removed");
				}
			if(country == "India" && postLen == 6){
				jQuery('#postalMessage').html('<span style="color:darkgreen;">That postal code looks good!</span>');
				localStorage.setItem("postalPassed", true);
				}
		jQuery('#postal').val(postalCode);
	}); // end on change
	}// end postalCheck

function phoneValidate(){
	jQuery("#phone").on("change",function(){
		var phoneNum = jQuery("#phone").val();
		var phoneNumCleaned = phoneNum.replace(/\D/g,'');
		var phoneLen	= phoneNumCleaned.length;
		console.log("Phone #: "+phoneNumCleaned);
		console.log("Phone Length: "+phoneLen);
		var phoneVal = jQuery('#phoneCountry :selected').val();
			if(phoneVal == 1){
				console.log("Len must be 7");
				if(phoneLen !== 10){
					jQuery("#phoneMessage").html('<span style="color:red;">Phone # does not look right!</span>');
					localStorage.removeItem("phonePassed"); 
					jQuery("#tosAgree").prop('checked', false);
					} else {
					jQuery("#phoneMessage").html('<span style="color:darkgreen;">Phone # looks good!</span>');
					localStorage.setItem("phonePassed", true);
					localStorage.setItem("noPhone", false);
					}
			}
		if(phoneVal == 91){
				console.log("Len must be 8-10");
				if(phoneLen <= 7){
					jQuery("#phoneMessage").html('<span style="color:red;">Phone # does not look right!</span>');
					localStorage.removeItem("phonePassed"); 
					jQuery("#tosAgree").prop('checked', false); 
					} else {
					jQuery("#phoneMessage").html('<span style="color:darkgreen;">Phone # looks good!</span>');
					localStorage.setItem("phonePassed", true);
					localStorage.setItem("noPhone", false);
					}
			}
		if(phoneVal == 49){
			console.log("Len must be 10");
			if(phoneLen !== 10){
					jQuery("#phoneMessage").html('<span style="color:red;">Phone # does not look right!</span>')
					localStorage.removeItem("phonePassed"); 
					jQuery("#tosAgree").prop('checked', false);
					} else {
					jQuery("#phoneMessage").html('<span style="color:darkgreen;">Phone # looks good!</span>');
					localStorage.setItem("phonePassed", true);
					localStorage.setItem("noPhone", false);
					}
			}		
		});// end on change
	}// end phoneValidate

function tosCheck(){
	jQuery("#tosAgree").on("change", function(){
		jQuery("#tosMessage").html('')
		var c = this.checked ? true : false;	
		if(c == true) {console.log("Checked!")
		var phoneComplete = 'false';
		var passPass = localStorage.getItem("passwordPassed");
		console.log("passPass: "+passPass);
		var postPass = localStorage.getItem("postalPassed");
		console.log("postPass: "+postPass);
		var phonePass = localStorage.getItem("phonePassed");
		console.log("phonePass: "+phonePass);
		var noPhone = localStorage.getItem("noPhone");
		console.log("noPhone after check: "+noPhone);
		var badPass = window.localStorage.getItem("badPass");
		console.log("badPass: "+badPass);		
		if(noPhone == 'true'){var type = 2;} else if(noPhone == 'false' || !noPhone){var type = 0;}
		console.log("Type: "+type);
		// if all looks good and user has a phone, show the create account button			
		if(passPass == 'true' && postPass == 'true' && phonePass == 'true' && badPass == 'true' && type == 0){
			console.log("Everything looks good!");
			console.log("Ready to submit account info!");			
			submitNewAccountInfo(0);
			}// end if all looks good!
		// if all looks good but no phone...
		else if(passPass == 'true' && postPass == 'true' && phonePass == 'true' && badPass == 'true' && type == 2){
			console.log("Everything looks good, but you don't have a phone :(");
			console.log("Ready to submit account info!");			
			submitNewAccountInfo(2);
			}// end if all looks good!
		else if(passPass == null || postPass == null || phonePass == 'false' || badPass == 'false' ){
			passingGrade = 0;
			if(passPass == null){
				jQuery("#tosMessage").append('<p style="color:red;">&#149; Your password isn\'t right.</p>')
				};
			if(postPass == null){
				jQuery("#tosMessage").append('<p style="color:red;">&#149; Your postal code needs to be corrected.</p>')
				};
			if(phonePass == null && noPhone == 'false'){
				jQuery("#tosMessage").append('<p style="color:red;">&#149; Your phone info is incomplete.</p>')
				};
			if(badPass == 'false' || badPass == 'false'){
				jQuery("#tosMessage").append('<p style="color:red;">&#149; Your password is incomplete.</p>')
				};	
		jQuery("#tosAgree").prop('checked', false);
		} // end if one of the session vars is null				

		} // end if checked

	if(c == false) {
		console.log("Unchecked!")
		jQuery("#submit-button-wrapper").css("display","none")
		}// end if
		});		
	} // end tosCheck
	
function noPhone(){ }

		jQuery("#noPhone").on("change", function(){
		var p = this.checked ? true : false;	
			if(p == true) {
				console.log("No Phone checked!");
				localStorage.setItem("noPhone", true);	
				localStorage.setItem("phonePassed", true);	
				jQuery("#phoneCountry").prop('disabled', 'disabled');
				jQuery("#phoneMessage").html('');
				}	
			else if(p == false) {
				console.log("No Phone unchecked!");
				localStorage.removeItem("noPhone");
				jQuery("#phoneCountry").prop('disabled', false);
				jQuery("#tosAgree").prop('checked', false);
				//jQuery("#submit-button-wrapper").css("display","none")
				}
		}) // end if one of the session vars is null	
	// temp remove?} // end noPhone

function submitNewAccountInfo(type){
	console.log(type)
	//clearAccountSessionInfo();
	var newUserInfo = '';
	var hash = '';
	var nuid = '';
	noPhone  = localStorage.getItem("noPhone");	
	console.log("noPhone value: "+noPhone);

// No Phone account creation
	if(type == 2){
	jQuery("#submit-button-wrapper-no-phone").slideDown("fast");
		console.log("Submit button enabled no phone");	
		jQuery("#submit-button-no-phone").on("click",function(){
		jQuery("#loading").css("display","block");
		console.log("Submit button clicked (no phone)!");
		var year = jQuery("#year-test :selected").val();
		var user = jQuery("#newUserName").val();
		var emal = jQuery("#newUserEmail").val();
		var pass = jQuery("#password").val();
		var coun = encodeURIComponent(jQuery("#countrySelect :selected").val());
		var post = jQuery("#postal").val();
		var parn = jQuery("#parent :selected").val();		
		
		var create_user_url = 'http://www.shareatalent.com/api/createuser/createUser_api/?username='+user+'&pass='+pass+'&email='+emal+'&tos=true&year='+year+'&postal='+post+'&country='+coun+'&parent='+parn+'&phone=0&phonecountry=0&user_activation_type=2';
			console.log("Create User URL: "+create_user_url);

			jQuery("#submit-button-message").html('<h3>Creating Account!</h3>');
			jQuery("#submit-button-message-no-phone").slideDown("fast");
			jQuery("#go-back-to-login-button").hide();

			jQuery.getJSON(create_user_url,
						function(newUser){																	
							var newUserInfo = JSON.stringify(newUser);
							console.log(newUser)
							console.log('newUserInfo: ' + newUserInfo);
							console.log('Status: ' + newUser.status);
							console.log('UserID: ' + newUser.user_id);
							console.log('Hash: ' + newUser.hash);
							console.log('User: ' + newUser.username);
							console.log('Email: ' + newUser.email);
							console.log('Type: ' + newUser.type);
							var nuid = newUser.user_id;
							localStorage.setItem("nuid", newUser.user_id);
							localStorage.setItem("satUserID", newUser.user_id);
							localStorage.setItem("sat_hash", newUser.hash);
							localStorage.setItem("sat_username", newUser.username);
							localStorage.setItem("satUserEmail", newUser.email);
							localStorage.setItem("sat_type", newUser.type);									
						}// end function
					).always(function(){
						jQuery("#loading").hide()
						} // end .always
					).done(function(){
					console.log("Create account success!");					
					var satType = localStorage.getItem("sat_type");
					jQuery("#submit-button-message-no-phone").html('<a style="color:red;"href="activate.html" data-role="button">Activate Your Account!</a>').trigger("create");					
					jQuery("#submit-button-no-phone-wrapper").slideUp("fast");
						} // end .done
				)// end getJSON
			
		})// end click
		
			
	} else if(type == 0) {

// Phone account creation
	jQuery("#submit-button-wrapper").slideDown("fast");
			console.log("Submit button enabled");
	jQuery("#submit-button").on("click",function(){		
		console.log("Submit button clicked!");
		var year = jQuery("#year-test :selected").val();
		var user = jQuery("#newUserName").val();
		var emal = jQuery("#newUserEmail").val();
		var pass = jQuery("#password").val();
		var coun = jQuery("#countrySelect :selected").val();
		var post = jQuery("#postal").val();
		var phco = jQuery("#phoneCountry :selected").val();
		var phon = jQuery("#phone").val();
		console.log("year: "+year);
		console.log("user: "+user);
		console.log("emal: "+emal);
		console.log("pass: "+pass);
		console.log("coun: "+coun);
		console.log("post: "+post);
		console.log("phco: "+phco);
		console.log("phon: "+phon);	
		jQuery("#loading").show();
		//alert("year: "+year+" user: "+user+" emal: "+emal+" pass: "+pass+" coun: "+coun+" post: "+post+" phco: "+phco+" phon: "+phon);	

		var create_user_url = 'http://www.shareatalent.com/api/createuser/createUser_api/?username='+user+'&pass='+pass+'&email='+emal+'&tos=true&year='+year+'&postal='+post+'&country='+coun+'&phone='+phon+'&phonecountry='+phco+'&user_activation_type=0';

			jQuery("#submit-button-message").html('<h3>Creating Account!</h3>');
			jQuery("#submit-button-message").slideDown("fast");
			jQuery("#go-back-to-login-button").hide();

			jQuery.getJSON(create_user_url,
						function(newUser){ 
							jQuery("#submit-button-message").html('<h3>Creating Account!</h3>')
							var newUserInfo = JSON.stringify(newUser);
							
							console.log(newUser)
							console.log('newUserInfo: ' + newUserInfo);
							console.log('Status: ' + newUser.status);
							console.log('UserID: ' + newUser.user_id);
							console.log('Hash: ' + newUser.hash);
							console.log('User: ' + newUser.username);
							console.log('Email: ' + newUser.email);
							console.log('Type: ' + newUser.type);
							var nuid = newUser.user_id;
							localStorage.setItem("nuid", newUser.user_id);
							localStorage.setItem("satUserID", newUser.user_id);
							localStorage.setItem("sat_hash", newUser.hash);
							localStorage.setItem("sat_username", newUser.username);
							localStorage.setItem("sat_email", newUser.email);
							localStorage.setItem("sat_type", newUser.type);			
						}// end function
					).always(function(){
						jQuery("#loading").hide()}
					).done(function(){
						var satType = localStorage.getItem("sat_type");
					jQuery("#submit-button-message").html('<a style="color:red;" href="activate.html" data-role="button">Activate Your Account!</a>').trigger("create");
					jQuery("#submit-button-phone-wrapper").slideUp("fast");
					
						
						}); // end getJSON	
	});// end submitNewAccountInfo	

	}}

// ***********************

function createAccountCredentials(){
	jQuery("#newUserName").on("click",function(){
		jQuery("#go-back-to-login-button").css("margin-bottom","500px");
		console.log("Attempting to expand the div for Android");
		jQuery('html, body').animate({
			scrollTop: jQuery('#newUserName').offset().top - 20
		});
	});
				//checkLoginInfo();
				var newUserName 	= '';
				var newUserEmail 	= '';
				console.log("Ready to test new account values.")
				
				jQuery('#get-started-button').on("click",function(event){
					event.preventDefault();
					jQuery("#go-back-to-login-button").css("margin-bottom","25px");
					jQuery("#checking-dups").slideDown("fast");
					console.log("check availability button clicked");
					jQuery("#new-user-message").html('');
					var newUserName 	= jQuery('#newUserName').val();
					var newUserEmail 	= jQuery('#newUserEmail').val();
					console.log("name: "+newUserName+" Email: "+newUserEmail);
					var lengthUser		= newUserName.length;
					var lengthEmail 	= newUserEmail.length;
					var lengthBoth 		= lengthUser + lengthEmail;
					//console.log(" lengthUser: "+lengthUser+" lengthEmail: "+lengthEmail+" lengthBoth: "+lengthBoth)
					if(lengthUser <= 1 || lengthEmail <= 1){
						//console.log("Nothing entered!");
						jQuery("#new-user-message").html('<p style="color:red;">You can\'t leave Username or Email blank...</p>'+
						'<p style="color:red;">Hint: Filling out forms is not your talent</p>');						
						} else {
						//console.log("name: "+newUserName+" Email: "+newUserEmail);
						validEmail(newUserEmail);
						}// end else 
						}) 
				}// end 
			
			function validEmail(v) {
				//console.log("testing email: "+v)    			
				var hasAt = v.indexOf('@');
				var hasDot = v.indexOf('.');
				var varsAdded = hasAt + hasDot;
				//console.log(varsAdded);
				if(varsAdded >= 1 && hasAt >= 1 && hasDot >= 1) {
					ajaxCheckDups();
					} else {
						jQuery("#new-user-message").html('<span style="color:red;">That email doesn\'t look right...</span>');
						}
function ajaxCheckDups(){
	console.log("Email looks good. Time to see if it is a dup");	
	//jQuery("#get-started-button").attr("disabled",true);
	var newUserName 	= jQuery('#newUserName').val();
	var newUserEmail 	= jQuery('#newUserEmail').val();
	var dupCheckURL 	= 'http://www.shareatalent.com/api/createuser/checkUsernameEmail_api/?user='+newUserName+'&eml='+newUserEmail;
	console.log(dupCheckURL);
jQuery.ajax({
	url: dupCheckURL,
	type: 'get',
	dataType: 'jsonp',
	//data: {user: newUserName, eml: newUserEmail},
	beforeSend: function(){jQuery('#loading').show()},
	success: function(result){	
		console.log("Success on Ajax");
		jQuery("#checking-dups").slideUp("fast");
		dupCheckResults(result);
		if(result.status == "error"){
			jQuery("#new-user-message").html('<h3 style="color:red;">'+result.message+'</h3>').trigger("create");
			jQuery("#show-form").slideUp("fast")			
			} else {
			var newUserName 	= jQuery('#newUserName').val();
			var newUserEmail 	= jQuery('#newUserEmail').val();									
			
			jQuery("#new-user-message").html(result.message);
			if(result.message == 'That Username is already in use. Try another!'){
				jQuery("#show-form").slideUp("fast");
				} else {
				jQuery("#show-form").slideDown("fast");
				
				console.log('Slide Up to show-form');
			jQuery('html, body').animate({
				scrollTop: jQuery('#show-form').offset().top - 20		
				});	
				
				jQuery("#get-started-button").slideUp("fast");
			} // else
				
			}
		
		},
	error: function(){
		console.log("Error on Ajax");
		console.log(result);
		},
	});
	function dupCheckResults(result){
		console.log("Successful Ajax!");
		jQuery('#loading').hide();
		console.log(result);
		}
						}// end aJaxCheckDups

jQuery("#countrySelectLabel").on("click",function(){
	jQuery("#country-question").slideToggle("")
	});
jQuery("#postalCodeLabel").on("click",function(){
	jQuery("#postal-code-question").slideToggle("")
	});
jQuery("#parentAccountLabel").on("click",function(){
	jQuery("#parent-question").slideToggle("")
	});	
jQuery("#cellPhoneLabel").on("click",function(){
	jQuery("#cell-phone-question").slideToggle("")
	});	

jQuery("#take_pic").click(function(event){
	event.preventDefault();
	if (!navigator.camera) {
	  alert("Camera API not supported", "Error");
	  return;
	}
	var options =   {   quality: 50,
	  destinationType: Camera.DestinationType.DATA_URL,
	  sourceType: 1,      // 0:Photo Library, 1=Camera, 2=Saved Album
	  encodingType: 0     // 0=JPG 1=PNG
	};

	navigator.camera.getPicture(
	function(imgData) {
        //alert(imgData);
        var smallImage = document.getElementById('thumb_preview');
        smallImage.src = "data:image/jpeg;base64," + imgData;
        document.getElementById("image_src").value = smallImage.src;
	},
	function() {
	  alert('Error taking picture', 'Error');
	},
	options);

	return false;
});	
	
                
jQuery("#upload_pic").click(function(event){
   event.preventDefault();
	if (!navigator.camera) {
	  alert("Camera API not supported", "Error");
	  return;
	}
	var options =   {   quality: 50,
	  destinationType: Camera.DestinationType.DATA_URL,
	  sourceType: 0,      // 0:Photo Library, 1=Camera, 2=Saved Album
	  encodingType: 0     // 0=JPG 1=PNG
	};

	navigator.camera.getPicture(
	function(imgData) {
        //alert(imgData);
        var smallImage = document.getElementById('thumb_preview');
        smallImage.src = "data:image/jpeg;base64," + imgData;
        document.getElementById("image_src").value = smallImage.src;
	},
	function() {
	  alert('Error taking picture', 'Error');
	},
	options);

	return false;
});   

}// end createAccountCredentials



