// JavaScript Document
jQuery.noConflict();

/*************************/
/***** Primary Level *****/
/*************************/
jQuery(document).ready(function($) 
{
	jQuery("#mypage-nav").on("vclick",function(){
		
		var UserID = "";

/**** Check to see if user already logged in ****/
	var UserID = window.localStorage.getItem("satUserID");	
	if (UserID>0) {		
		/*
		jQuery("#content").css("display","block");		
		jQuery("#login-form").css("display", "none");
		jQuery("#login-form").css("display", "none");		
		jQuery("#user-id div").text(UserID);
		*/
		cl('Already logged in as User#: '+UserID);	
		//display member's page
			var userURL = 'http://www.shareatalent.com/api/userprofile/userProfile_api/?userid='+UserID+'&callback=?';	
				jQuery.getJSON(userURL,
				function(userProfile){ 
					var userProf = JSON.stringify(userProfile);
					//cl('Profile: ' + userProf);
					//jQuery("#profile").html(userProfile.html);
					/* Profile Header Content */
					jQuery("#gravatar").html(userProfile.user.gravatar).trigger("create");
					jQuery("#displayname").html(userProfile.user.displayname).trigger("create");
					jQuery("#talent_1_top").html(userProfile.user.talent_1_top).trigger("create");
					jQuery("#talent_1_lv2").html(userProfile.user.talent_1_lv2).trigger("create");
					jQuery("#talent_1_name").html(userProfile.user.talent_1_name).trigger("create");
					
					/* Profile Options Content */
					jQuery("#endoresment_count").html(userProfile.user.endoresment_count).trigger("create");
					//jQuery("#activity_count").html(userProfile.user.activity_count).trigger("create");		
					jQuery("#friend_count").html(userProfile.user.friend_count).trigger("create");
					jQuery("#friend_request_count").html(userProfile.user.friend_request_count).trigger("create");
					
					/* About Talent Content */
					jQuery("#talent_1_about").html(userProfile.user.talent_1_about).trigger("create");					
					

					//jQuery("#user-settings").html(userProfile.html_panel);
						
						window.location.assign("index.html#main-profile");
				});		
		}
		
		})
	
	});