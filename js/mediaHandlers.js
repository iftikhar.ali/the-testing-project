document.addEventListener('deviceready', masterAudioCode, false);

var my_recorder = null; 
var my_player = null;

function masterAudioCode(){
window.localStorage.setItem("sat_recording",false);
var my_recorder = 'created'; 
var my_player = null;
cl("masterAudioCode running");
var progressTimmer = null;
var createdStatus = false;
var recTime = 0;

var d = new Date();
		y 	= d.getFullYear();
		m 	= d.getMonth()+1;
		da 	= d.getDay();
		h 	= d.getHours();
		mn 	= d.getMinutes();
		s 	= d.getSeconds();
		ml 	= d.getMilliseconds();
var unique 	= y+""+m+""+da+""+h+""+mn+""+s+""+ml;

function makeUnique(){
	var d = new Date();
		y 	= d.getFullYear();
		m 	= d.getMonth()+1;
		da 	= d.getDay();
		h 	= d.getHours();
		mn 	= d.getMinutes();
		s 	= d.getSeconds();
		ml 	= d.getMilliseconds();
var unique 	= y+""+m+""+da+""+h+""+mn+""+s+""+ml;
	return unique;
	}

// for recording: do not specify any directory
var mediaFileFullName = null;
var type_ios = ua.match(/(iphone|ipod|ipad)/i);
var type_android = ua.match(/android/i);
cl("type_ios: "+type_ios+" type_android: "+type_android);
if(type_ios){var mediaRecFile = "satrecording100.wav"; window.localStorage.setItem("sat_recording_name",mediaRecFile);}
else if (type_android){var mediaRecFile = "satrecording"+unique+".amr";window.localStorage.setItem("sat_recording_name",mediaRecFile);}
else if (!type_android && !type_ios){var mediaRecFile = "satrecording1000.wav";}
cl("mediaRecFile Line 18: "+mediaRecFile);
var checkFileOnly = false;
var mediaFileExist = false;
var myMediaState = 
{
	start: 1, 
	recording: 2, 
	finishRec: 3, 
	playback: 4, 
	paused: 5,
	stopped: 6 
};

if(typeof console === "undefined") {
    console = { log: function() { cl("console === undefined") } };
}   

function onOK_GetFile(fileEntry) {
    cl("***test: onOK_GetFile File name and path: " + mediaRecFile + " at " + fileEntry.fullPath);
    
    // save the full file name
    mediaFileFullName = fileEntry.fullPath;
    if (phoneCheck.ios)
        mediaRecFile = mediaFileFullName;

    if (checkFileOnly == true) { // check if file exist at app launch. 
        mediaFileExist = true;
        
        setButtonState(myMediaState.finishRec);
    } 
    else { 
        
        // create media object using full media file name 
        my_recorder = new Media(mediaRecFile, onMediaCallSuccess, onMediaCallError);

        // specific for iOS device: recording start here in call-back function
        cl("!!!! About to record !!!!");
		recordNow();
		
    }
}

function onSuccessFileSystem(fileSystem) {
    cl("***test: fileSystem.root.name: " + fileSystem.root.name);
	
	if (checkFileOnly == true) {
		cl("checkFileOnly == true | mediaRecFile: " +mediaRecFile);
        fileSystem.root.getFile(mediaRecFile, { create: false, exclusive: false }, onOK_GetFile, 
		function(){cl("!!!! error checkFileOnly == true")});
	} else {
        fileSystem.root.getFile(mediaRecFile, { create: true, exclusive: false }, onOK_GetFile,  function(){cl("checkFileOnly else called")});
	}	
}
function checkMediaRecFileExist() {
        cl("checkMediaRecFileExist triggered");
    checkFileOnly = true;
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onSuccessFileSystem, onFailedFileSystem);
}

function onFailedFileSystem(){cl("!!!! Failed the checMediaRecFilExist test");}

function recordNow() {
        cl("recordNow triggered");        
        jQuery("#start-recording-audio-container").slideUp("fast");
        jQuery("#audio-recording-container").slideDown("fast");
        jQuery("#capture-photo-button").slideUp("fast");
        jQuery("#open-images-folder-button").slideUp("fast");

	 	my_recorder = new Media(mediaRecFile, onMediaCallSuccess, onMediaCallError);
        console.log(mediaRecFile);
        my_recorder.startRecord();
        cl("Status: recording");
        cl("***test:  recording started: in recordNow() line 104 ***");
   
}// recordNow

function recordNowAndroid(){
	cl("recordNowAndroid triggered");	
	window.localStorage.setItem("sat_recording",true);	
		     
        jQuery("#start-recording-audio-container").slideUp("fast");
        jQuery("#audio-recording-container").toggle();
        jQuery("#capture-photo-button").slideUp("fast");
        jQuery("#open-images-folder-button").slideUp("fast");
		var src = "satrecording"+unique+".amr";
        var my_recorder = new Media(src, onMediaCallSuccess, onMediaCallError,function(status){
			var sat_recorder = JSON.stringify(my_recorder);
			console.log(sat_recorder);
			localStorage.setItem("sat_recorder", sat_recorder);
			console.log("Media status:");
			console.log(status)});
		console.log("my_recorder (Android):")
		console.log(my_recorder);
        // Record audio
        my_recorder.startRecord();		
		
		// temp stop
		var recTime = 0;
        var recInterval = setInterval(function() {
            recTime = recTime + 1;
            setAudioPosition(recTime + " sec");
			recording = window.localStorage.getItem("sat_recording");
            if (recTime >= 5000 || recording == "false") {
                clearInterval(recInterval);
                my_recorder.stopRecord();
            }
        }, 1000);
		
	}// recordNowAndroid

// Record audio    
//     
jQuery("#record-audio").on("click",hereWeGo);
function hereWeGo(){
	navigator.notification.confirm("Ready to record?",startTheProcess,"Ready, Set...","Record!, Nope!");
	}

function startTheProcess(){		
        cl("Record button pressed");
        checkMediaRecFileExist();
        cl("checkMediaRecFileExist triggered from record-audio on click line 148");
        startRecording();
}// start the process


function startRecording() {
        cl("startRecording triggered")
    // change buttons state
    setButtonState(myMediaState.recording);
        cl("setButtonState triggered");
    // create media object - overwrite existing recording
    if (my_recorder)
        //my_recorder.release();
                cl("my_recorder.release");
    if (phoneCheck.android) {		
		cl("mediaRecFile Line 125: "+mediaRecFile); 
        cl("***test: new Media() for android ***");

        recordNowAndroid();
    }
    else if (phoneCheck.windows7) {
        my_recorder = new Media(mediaRecFile, onMediaCallSuccess, onMediaCallError);
        cl("***test: new Media() for Windows7 ***");

        recordNow();
    }
    else if (phoneCheck.ios) {                
        //first create the file
        checkFileOnly = false;
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onSuccessFileSystem, function() {
            cl("***test: failed in creating media file in requestFileSystem");
        });

        cl("***test: new Media() for ios***");
    }
    
}

// Stop recording
jQuery("#stop-recording-audio").on("click",function(){
        cl("stop recording button pressed");
        jQuery("#audio-play-button-container").slideDown("fast");
        jQuery("#audio-recording-container").slideUp('fast');
		window.localStorage.setItem("sat_recording",false);		
		cl("MediaRecFile: "+ MediaRecFile)
        stopRecording();

        });

function stopRecording() {
	if(phoneCheck.android && my_recorder == null){
		sat_recorder = localStorage.getItem("sat_recorder"); 
		console.log("sat_recorder:");
		console.log(sat_recorder);
		my_recorder = sat_recorder
		}  
	console.log("my_recorder");	
	console.log(my_recorder);
    // enable "record" button but disable "stop"
    //clearProgressTimmer();
    //setButtonState(myMediaState.finishRec);
	cl("stopRecording triggered - line 199");
    if (my_recorder) {
        my_recorder.stopRecord(); // the file should be moved to "/sdcard/"+mediaRecFile
		cl("stopRecord()");
		my_recorder.release();
		cl("release()")
    	clearProgressTimmer();

    cl("Status: stopped record - line 208");
    cl("***test: recording stopped***");
	} else {console.log("Can't find 'my_recorder'")}
}// stopRecording

// Play audio        
jQuery("#audio-play-button-container").on("click", function(){
        cl("Play button pressed");
        jQuery("#audio-play-button-container").slideUp("fast");
        jQuery("#stop-playing-audio-container").slideDown('fast');
        playMusic();
        });
function playMusic() {
        cl("playMusic function started");        
    if (my_player === null) { // play existing media recorded from previous session
        
        // the existing medail should be on /sdcard/ for android. 
        if (phoneCheck.android) {
			cl("Play!! phoneCheck.android");
            my_player = new Media("satrecording"+unique+".amr", onMediaCallSuccess, onMediaCallError);
			//my_player = new Media("/sdcard/" + mediaRecFile, onMediaCallSuccess, onMediaCallError);

            cl("***test:  Open file:" + mediaRecFile);
        } else if (phoneCheck.windows7) // windows 7.1 phone
            my_player = new Media(mediaRecFile, onMediaCallSuccess, onMediaCallError);
        else if (phoneCheck.ios) {
            my_player = new Media(mediaFileFullName, onMediaCallSuccess, onMediaCallError);
        }
    }

    // Play audio
    if (my_player) {
        my_player.play();
        cl("Status: playing...");

        setButtonState(myMediaState.playback);

        // Update media position every second
        clearProgressTimmer();
        progressTimmer = setInterval(function () {
            // get my_player position
            my_player.getCurrentPosition(
            // success callback
            function (position) {
                if (position >= 0)
                    setAudioPosition('media_pos', (position) + " sec");
                else {
                    // reached end of media: same as clicked stop-music 
                    clearProgressTimmer();
                    setAudioPosition('media_pos', "0 sec");
                    cl("Status: stopped");
                    setButtonState(myMediaState.stopped);
                }
            },
            // error callback
            function (e) {
                cl("Status: Error on getting position - " + e);
                setAudioPosition("Error: " + e);
            });
        }, 1000);
    }
}
// Pause audio
//
function pauseMusic() {
    if (my_player) {
        my_player.pause();
        cl("Status: paused");

        clearProgressTimmer();
        setButtonState(myMediaState.paused);
    }
}
// Stop audio        
// 
jQuery("#stop-playing-audio").on("click",function(){
        jQuery("#audio-play-button-container").slideDown("fast");
        jQuery("#stop-playing-audio-container").slideUp('fast');
        cl("Stop button pressed");
        stopMusic();
        });
function stopMusic() {
        cl("stopMusic function started");        
    if (my_player) {
        setAudioPosition('media_pos', "0 sec");
        setButtonState(myMediaState.stopped);

        my_player.stop();

        // should not be necessary, but it is needed in order to play again. 
        my_player.release()
        my_player = null; 

        clearProgressTimmer();
        cl("Status: stopped");
    }
}
function clearProgressTimmer() {
    if (progressTimmer) {
        clearInterval(progressTimmer);
        progressTimmer = null;
    } 
}
// Media() success callback        
function onMediaCallSuccess() {
    createdStatus = true;
    cl("***test: new Media() succeeded ***");
}
// Media() error callback        
function onMediaCallError(error) {
    console.log('line 315 - code: '    + error.code + ' | message: ' + error.message);
	cl("***test: new Media() failed ***");
}
// Set audio position        
//
function setAudioPosition(audioPosID, position) {
    //document.getElementById(audioPosID).innerHTML = "<p></p>Audio position: "+position;
        cl("Audio position: "+position);
}

// only "Record" button is enabled at init state
function setButtonState(curState)
{
    var id_disabled_map = {"startRecID":false, 
                             "stopRecID":true, 
                             "startPlayID":true, 
                             "pausePlayID":true, 
                             "stopPlayID":true};

    if (curState == myMediaState.start) // only "record" is enabled
    {
        cl("***test:  start state #####");
    }                         
    else if (curState == myMediaState.recording) // only "stoprec" is enabled
    {
        cl("***test:  recording state #####");
        id_disabled_map["startRecID"] = true;
        id_disabled_map["stopRecID"] = false;
    }
    else if ((curState == myMediaState.finishRec) ||
        (curState == myMediaState.stopped)) // only "record", "play" are enabled
    {
        cl("***test:  finishing/stopped state #####");
        id_disabled_map["startPlayID"] = false;
    }
    else if (curState == myMediaState.playback)  // only "pause", "stop" are enabled
    {
        cl("***test:  playback state #####");
        id_disabled_map["startRecID"] = true;
        id_disabled_map["startPlayID"] = true;
    }
    else if (curState == myMediaState.paused)  // only "play", "record" & "stop" are enabled
    {
        cl("***test:  paused state #####");
        id_disabled_map["startPlayID"] = false;
        id_disabled_map["stopPlayID"] = false;
    }
    else
    {
        cl("***  unknown media state");
    }

    var keys = Object.keys(id_disabled_map); //the list of ids: ["startRecID", "stopRecID",...]
        console.log(keys);
        
        id_disabled_map["startRecID"] = false;
        id_disabled_map["stopRecID"] = false;
        id_disabled_map["startPlayID"] = false;
        id_disabled_map["pausePlayID"] = false;
        id_disabled_map["stopPlayID"] = false;
    /*
        keys.forEach(function(id){ 
                console.log(id);
                //id_disabled_map[id] = false;                
        });
        */
        console.log(id_disabled_map);
    return(id_disabled_map); 
}

}// masterAudioCode
//https://github.com/apache/cordova-plugin-media/blob/6de7e53f40be0160dff9789e98ad664574ca6ff4/docs/media.play.md
// from 0.092s
