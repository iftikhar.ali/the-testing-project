// http://jquerymobile.com/demos/1.1.1/docs/pages/page-dynamic.html

jQuery(document).bind( "pagebeforechange", function( e, data ) {
var UserID = window.localStorage.getItem("satUserID");
var category;
	if ( typeof data.toPage === "string" ) {
		var u = jQuery.mobile.path.parseUrl( data.toPage ),
			re = /^#member-page/;

		if ( u.hash.search(re) !== -1 ) {
			showCategory( u, data.options );
			e.preventDefault();
		}
	}
});

function showCategory( urlObj, options )
{
	var categoryName = urlObj.hash.replace( /.*memberId=/, "" ),
	
		// re-used categoryName for simplicity
		category = categoryName,
		
		// Page we're going to - #member-page		
		pageSelector = urlObj.hash.replace( /\?.*$/, "" );
		jQuery("#member-info").empty();
	if ( category ) {
		// Get the page we are going to dump our content into.
		var memberProfile;
		var sat_markup;		
			
		var $page = jQuery( pageSelector ),

			// Get the header for the page.
			$header = $page.children( ":jqmData(role=header)" ),

			// Get the content area element for the page.
			$content = $page.children( ":jqmData(role=content)" )
			var memberContent = "member-page";
		
		var UserID = window.localStorage.getItem("satUserID");
		var userID = window.localStorage.getItem("satUserID");
		var sat_markup = '';
		memberURL = 'http://www.shareatalent.com/api/userprofile/memberId_api/?memberid='+category+'&userid='+UserID;
		cl(memberURL);
		jQuery.getJSON(memberURL,
				function(memberProfile){ 
					console.log("memberProfile");
					console.log(memberProfile);
					var userProf = JSON.stringify(memberProfile);
					//cl('Profile: ' + userProf);
					//jQuery("#profile").html(memberProfile.html);
					
					/* Profile Header Content */
										
					sat_markup = 	'<div id="profile-content">';
					sat_markup +=  	'<div id="this-member-id" style="display:none;">'+category+'</div>';
					sat_markup += 	'<div id="profile-header">';
					sat_markup += 	'<div id="gravatar">'+memberProfile.user.gravatar+'</div>';	
					sat_markup += 	'<div id="name-talent-highlight">';
					sat_markup += 	'<h2 id="member-name">'+memberProfile.user.displayname+'</h2>';
					sat_markup += 	'<h4><span id="talent_1_top">'+memberProfile.user.talent_1_top+'</span>->';
					sat_markup += 	'<span id="talent_1_lv2">'+memberProfile.user.talent_1_lv2+'</span>->';
					sat_markup += 	'<span id="talent_1_name">'+memberProfile.user.talent_1_name+'</span>';
					sat_markup +=	'<span id="endorsement_count"('+memberProfile.user.endoresment_count+')</h4>';		
					sat_markup += 	'</div></div>';
					if(memberProfile.user.talent_1_about){
					sat_markup +=	'<div id="member-about-talent">'+memberProfile.user.talent_1_about+'</div>';}// end if user has written about their talent					
					//cl(sat_markup);
					//$content.html( sat_markup );
					jQuery("#member-info").html(sat_markup);
					jQuery("#member-message").html('');

// Endorsements
var endorseURL = 'http://shareatalent.com/api/endorse_check.php?friend='+category+'&user_id='+userID+'&talent_id='+memberProfile.user.talent_1_id;
console.log("endurseURL: "+endorseURL);
jQuery.getJSON(endorseURL,function(d){
	console.log(d);
	if(d.status == 'ok'){
		jQuery("#member-endorse-button").html('<button onClick="unendorse('+category+', '+UserID+', '+memberProfile.user.talent_1_id+', \''+memberProfile.user.talent_1_name+'\', \''+memberProfile.user.displayname+'\')" id="endorse-friend-button-'+category+'">Endorsed <i style="color:green;" class="fa fa-check"></i></button>').trigger('create');
		jQuery("#member-endorse-button").show();
		}
	if(d.status == 'no'){
		jQuery("#member-endorse-button").html('<button onClick="endorse('+category+', '+UserID+', '+memberProfile.user.talent_1_id+', \''+memberProfile.user.talent_1_name+'\', \''+memberProfile.user.displayname+'\')" id="endorse-friend-button-'+category+'">Endorse</button>').trigger('create');
		jQuery("#member-endorse-button").show();
		}
	});	
	

			if(memberProfile.user.friend_check <= 0) {
				console.log('debugv1');
				jQuery('#member-messaging-button').empty();
				jQuery('#member-messaging-button').html('<a href="#friend-message" data-role="button" id="message-'+category+'">Send Message</a>').trigger('create');
				jQuery('#member-friend-button').html('<button onClick="addFriendButton('+userID+', '+category+')" id="add-friend-button-'+category+'">Add Friend</button>').trigger('create');						
						}// if no friend requests made

			else if(memberProfile.user.friend_check > 0 && memberProfile.user.friend_status == 1) {
				console.log('debugv2');
				jQuery('#member-messaging-button').empty();
				jQuery('#member-messaging-button').html('<a href="#friend-message" data-role="button" id="send-message">Send Message</a>').trigger('create');
				jQuery('#member-friend-button').empty();
				jQuery('#friends-message-id').text(category);
				jQuery("#member-delete").html('<button style="background-color:red;" onClick="deleteFriendButton('+category+', '+UserID+');">Delete Friend</button>').trigger("create");						
						}// if friend

			else if(memberProfile.user.friend_check > 0 && memberProfile.user.friend_status == 0) {
				console.log('debugv3');
				jQuery('#member-messaging-button').empty();
				jQuery('#member-messaging-button').html('<a href="#friend-message" data-role="button" id="send-message">Send Message</a>').trigger('create');
				jQuery('#friends-message-id').text(category);
				jQuery("#member-delete").html('<ul data-role="listview" style="margin:0;"><li style="text-align:center;">Friend Request Sent</li></ul>').trigger("create");						
						}// if friend request sent	

			else if(memberProfile.user.friend_check > 0 && memberProfile.user.initiator != UserID) {
				console.log('debugv4');
				jQuery('#member-messaging-button').empty();
				jQuery('#member-messaging-button').html('<a href="#friend-message" data-role="button" id="send-message">Send Message</a>').trigger('create');
				jQuery('#friends-message-id').text(category);
				jQuery('#member-friend-button').empty().trigger("create");
				jQuery("#member-delete").html('<button onClick="acceptFriendButton('+category+', '+UserID+');">Accept Friend Request</button>').trigger("create");	
						}// if they sent friend request
				// clear messages
				jQuery("#member-message").html('').trigger("create");
				});// end getJSON

		/* Activity Feed */
var activityURL = 'http://www.shareatalent.com/api/userprofile/userActivities_api/?userid='+category
jQuery.getJSON(activityURL,
	function(memberActivity){ 
		jQuery("#member-activity").empty();
		if(memberActivity.activities){
		cl(memberActivity.activities);
		jQuery.each(memberActivity.activities, function(result, value){
			if(value.content) {
				jQuery("#member-activity").append('<div class="activity-content">'+value.content+'</div><div id="thread-'+result+'"></div>').trigger("create");
			}// end if
			});// end each
		} // end if activities
		//cl("Comments: "+memberActivity.comments);
		if(memberActivity.comments){
		jQuery.each(memberActivity.comments, function(result, value){
			cl(value.item_id+" | "+value.content)
			});// each
		}// end if
	});// end getJSON
	
	jQuery("#send-the-message").on("click", function(){
		var messageTextSubject = encodeURIComponent(jQuery('#message-subject').val());
		var messageTextContent = encodeURIComponent(jQuery('textarea#message-contents').val());
		var sendToID = jQuery('#friends-message-id').text();
		var sendFrom = UserID;		
		var sendMessageURL = 'http://www.shareatalent.com/api/usersettings/sendMessage/?to='+sendToID+'&from='+UserID+'&sub='+messageTextSubject+'&mes='+messageTextContent
		console.log(sendMessageURL);
		jQuery.getJSON(sendMessageURL, function(data){
			if(data.status=='ok'){
				jQuery('#message-subject').val('');
				jQuery('textarea#message-contents').val('');
				jQuery("#friend-message").panel("close");
				jQuery("#member-message").html('<h3 style="color:red;">Message sent!</h3>').toggle().trigger("create");
				}// if
			})// getJSON
	});


		
	$header.find( "h1" ).html( category.name );

	//

	$page.page();

	$content.find( ":jqmData(role=listview)" ).listview();
	
	options.dataUrl = urlObj.href;
	jQuery.mobile.changePage( $page, options );
	jQuery("#member-page").trigger("page");
	}
}