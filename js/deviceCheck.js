document.addEventListener('deviceready', loadAudioControllers, false);

document.addEventListener("offline", onOffline, false);

document.addEventListener("backbutton", backKeyDown, true);

function backKeyDown() {
    if(jQuery.mobile.activePage.is('#main-profile')){
       navigator.notification.confirm(
            'Do you want to change, logout and exit from app. ?. Touch elsewhere to dismiss.', // message
             onBackConfirm,                     // callback to invoke with index of button pressed
            'Logout and exit from app ?',       // title
            ['RUN IN BACKGROUND','No thank\'s']     // buttonLabels
        );
    }
    else {
        navigator.app.backHistory()
    }
}

function onBackConfirm(buttonIndex) {
    if(buttonIndex > 0) {
        if(buttonIndex == 1) {
            navigator.app.exitApp();
        }
    }
}

function onOffline() {
    navigator.notification.alert(
        'Application is not connected to any network. You need to be connected to Wifi or any DATA network.',  // message
        alertDismissed,         // callback
        'No network found!',    // title
        'Okay, close app'       // buttonName
    );
}

function alertDismissed() {
    navigator.app.exitApp();
}


function loadAudioControllers() {
        //document.getElementById("PhonegapVerID").innerHTML += window.device.cordova;
        //cl("PhoneGap Version: " + window.device.cordova);
    
        cl("*** phoneCheck.android: " + phoneCheck.android);
        cl("*** phoneCheck.ios: " + phoneCheck.ios);
		cl("*** browserCheck: " + browserCheck.chrome);
    
        // set initial button state
        //setButtonState(myMediaState.start);
    
        // check if an existing media file already exist and reset button state
        //checkMediaRecFileExist();
    }

// detect device
var ua = navigator.userAgent.toLowerCase();

var phoneCheck = {
    ios: ua.match(/(iphone|ipod|ipad)/i),
    blackberry: ua.match(/blackberry/i),
    android: ua.match(/android/i),
    windows7: ua.match(/windows phone os 7.5/i)
};

// detect browser
var browserCheck = {
    chrome: ua.match(/chrome/i),
    ie: ua.match(/msie/i),
    firefox: ua.match(/firefox/i),
    safari: ua.match(/safari/i), //this one is the same as chrome. 
    opera: ua.match(/opera/i)
};

// detect HTML5 tag support
var myDeviceSupport = {
    HTML5_audio: !!(document.createElement('audio').canPlayType),
    HTML5_audio_mp3: !!(document.createElement('audio').canPlayType) && document.createElement('audio').canPlayType('audio/mpeg') != "",
    HTML5_audio_wav: !!(document.createElement('audio').canPlayType) && document.createElement('audio').canPlayType('audio/wav') != "",
    HTML5_geolocation: navigator.geolocation
};