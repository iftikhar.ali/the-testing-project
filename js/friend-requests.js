function loadFriendRequests(){
console.log("Friends Requests");
jQuery.mobile.loading('show');
jQuery("#list-of-friend-requests").empty();
var UserID = window.localStorage.getItem("satUserID");
friendRequestsURL = 'http://www.shareatalent.com/api/friend_requests.php?user='+UserID+'&type=requests';
jQuery.getJSON(friendRequestsURL,function(d){

if(d.status == 'ok'){
n=0;
jQuery.each(d.friend,function(){
	
	console.log(d.friend[n].first_name+' '+d.friend[n].last_name);
	jQuery("#list-of-friend-requests").append('<li>'+d.friend[n].first_name+' '+d.friend[n].last_name+'<button onClick="addNewFriend('+d.friend[n].friend_id+','+UserID+')">Add Friend</button></li>');
	jQuery("#list-of-friend-requests").trigger("create").listview("refresh");
	n++;
	});
	jQuery.mobile.loading('hide');
	
	}// if d
else if (d == 'undefined' || d.status == 'error'){jQuery("#friends-requesting-friendship").html('<h3>No Friend Requests at this time</h3>'); jQuery.mobile.loading('hide');}
});	

}// loadFriendRequests()


function addNewFriend(fid,userID){
	console.log("friendID: "+fid);
	console.log("userID: "+userID);
	addFriendURL = 'http://shareatalent.com/api/friend_requests.php?user='+userID+'&type=add&fid='+fid;
	jQuery.getJSON(addFriendURL,function(d){if(d.status=='ok'){
		loadFriendRequests();		
		} else {alert('Something went wrong with adding that friend. Try again later.')}
	});
	
	};// addNewFriend