document.addEventListener('deviceready', SearchFunctions, false);

function SearchFunctions(){
cl("Search Functions ready!");
	
jQuery.noConflict();
var UserID;


	var talentSearch = '';
	var UserID = window.localStorage.getItem("satUserID");	
	//cl("UserID: "+UserID);
	jQuery("#submit-search").on("vclick",function($){	
	cl("submit search clicked!");	
		jQuery("#search-messages").css("display","none");
		jQuery("#recommended-search").css("display","none");
		jQuery("#search-results-list-no-talent").empty();
		jQuery("#search-results-list").empty();
		jQuery("#recommended-search-list").empty();
		jQuery("#no-talent-header").css("display","none");
		cl("Buncha CSS got did!");	
		talentSearch = encodeURIComponent( jQuery("#search-page-bar").val() );
		cl(talentSearch);	
		// if an @ sign, searchType = 0
		// if not an @ AND some text, searchType = 1
		// if not an @ AND blank, searchType = 2
		if (talentSearch.indexOf("%40" || "@") >= 0) {var searchType = 0;}
		else{ 
			if(talentSearch != ""){var searchType = 1}
				else if(talentSearch == "") {var searchType = 2}
				};// if statements
				
		cl("Search Type: "+searchType);		
		
		if(searchType == 0) {searchTypeZero()
			} else { if(searchType == 1) {searchTypeOne()
				} else if(searchType == 2) {searchTypeTwo()}
//////////////////////////////////////////////////////////
		// Name Search 
		function searchTypeZero() {
			jQuery("#search-results-list").empty();
			//jQuery("#loading").css("z-index",99999);
			var memberSearchURL = 'http://www.shareatalent.com/api/talent/searchMembers_api/?userid='+
			UserID+'&search='+talentSearch;
			cl(memberSearchURL);
			jQuery.getJSON(memberSearchURL,function(searchResults){
				cl("User from search: "+searchResults.user);
				//cl("Talent from search: "+searchResults.results);
				jQuery.each(searchResults.results, function (id, name )
				{	
					var UserID = window.localStorage.getItem("satUserID");	
					if(name.userid !== UserID){
					console.log("UserID: "+UserID+" name.userid: "+name.userid);
					cl("Talent from search: "+name.talentid+"/"+name.name+"/ Type: "+name.type);
					if(name.type == "1"){						
						jQuery("#search-results-list").append(
						'<li><a href="#member-page?memberId='+name.userid+'" id="memberId-'+name.userid+'"><span class="message-header" style="font-size:12px;">'+
						name.name+' | '+name.talent_top+'->'+name.talent_sec+'->'+name.talent_nam+'</a></li>'
						).trigger("create").listview("refresh");
					}// end if
					else if(name.type == "0"){
						jQuery("#no-talent-header").css("display","block");
						jQuery("#search-results-list-no-talent").append(
						'<li><a href="#member-page?memberId='+name.userid+'" id="memberId-'+name.userid+'"><span class="message-header" style="font-size:12px;">'+
						name.name+' | '+name.talent_top+'</a></li>'
						).trigger("create");
						jQuery("#search-results-list-no-talent").listview("refresh");
						}// end else
					}//end if not the same user
				});// end for each
			});// end getJSON
		//jQuery("#loading").css("z-index",-99999);
		}// end function searchTypeZero

//////////////////////////////////////////////////////////
		// Talent Search
		function searchTypeOne() {
			jQuery("#search-results-list").empty();	
			//jQuery("#loading").css("z-index",99999);		
			var searchURL = 'http://www.shareatalent.com/api/findlocaltalent/localTalent_api/?userid='+UserID+'&search='+talentSearch;
			cl(searchURL);
					jQuery.getJSON(searchURL,function(searchResults){
					// number of recommended search results
					var recom = 0;
					jQuery.each(searchResults.recommend, function (user, info ){
						recom++;
						}); // end each recommended
					cl("Recommended Count: "+recom);
					if(recom == 0) {
						cl("No Results!");
						jQuery("#search-messages").css("display","block");	
						jQuery("#search-messages").html("<h3>No search results found. Check your spelling and try again.</h3>");	
					} // end if no matched results
					else if(recom == 1) {
						if(searchResults.results == null) {
							jQuery("#search-messages").css("display","block");
							jQuery("#search-messages").html("<h3>No Results for that Talent</h3>");
							} // end if null results
						else {
							jQuery.each(searchResults.results, function (user, info ){
								if(UserID !== info.user_id){
							console.log("UserID: "+UserID+" info.id: "+info.user_id)
							//jQuery("#recommended-item-number-"+recomItem).css("display","block");
							jQuery("#search-results-list").append('<li><a id="recomItemId-'+info.user_id+'" href="#member-page?memberId='+info.user_id+'">'+info.user_name+': '+info.user_city+', '+info.user_state+'</a></li>').trigger("create").listview("refresh");							
							recomItem++;
							}//end if not the same user								
							}); // end each recommended
						}// end if search results are present
					}// end if only one recommended talent
					else if(recom > 1) {					
					jQuery("#search-messages").css("display","block");
					jQuery("#search-results-list").css("display","block");																									
					//jQuery("#search-messages").html('<div><h3>Found several talent matches. Which of these is the best fit?</h3>');
					var recomItem = 0;					
					jQuery.each(searchResults.recommend, function (user, info ){	
						var UserID = window.localStorage.getItem("satUserID");	
						if(UserID !== info.id){
							console.log("UserID: "+UserID+" info.id: "+info.id)
							//jQuery("#recommended-item-number-"+recomItem).css("display","block");
							jQuery("#recommended-search-list").append('<li><a id="recomItemId-'+info.id+'" href="#">'+info.top_level+'->'+info.level_2+'->'+info.level_3+'</a></li>').trigger("create").listview("refresh");							
							recomItem++;
						}//end if not the same user
						});// end foreach result			
						jQuery("#recommended-search-list").trigger("create");
						jQuery("#recommended-search").trigger("create");
						jQuery("#search-page").trigger("pagecreate");	
						jQuery("#search-messages").css("display","block");
						jQuery("#recommended-search").css("display","block");									
						GetRecommendedTalent(UserID);

					}// end if recommended search results
				});
			}// end getJSON
		//jQuery("#loading").css("z-index",-99999);
		}// end function searchTypeOne	

//////////////////////////////////////////////////////////
		// If the search bar is blank...
		function alertDismissed() {
			// do something
		}
		function searchTypeTwo() {
			navigator.notification.alert(
				'Please enter any valid input in the search box. Ex. @John or @Dance',  // message
				alertDismissed,         // callback
				'Valid input required!',// title
				'Okay'                  // buttonName
			);
		}// end function searchTypeTwo	
	}); // end on Click search


//////////////////////////////////////////////////////////

function GetRecommendedTalent(UserID) {		
jQuery("a[id^='recomItemId']").on("vclick", function(){
	cl("This: "+this.id);
	var talentIdStr = this.id;
	var talentId = talentIdStr.replace('recomItemId-','');	
	cl("TalentID: "+talentId+" UserID: "+UserID);			
	
	var searchURL = 'http://www.shareatalent.com/api/findlocaltalent/localTalent_api/?userid='+UserID+'&talentid='+talentId;
				cl("searchURL: "+searchURL);
				jQuery.getJSON(searchURL,function(searchResults){
					//cl("Talent from Blank: "+searchResults.talentName);
					jQuery("#search-messages").html("<h3>Search Results for: <br />"+searchResults.talentTop+"->"+searchResults.talentSec+"->"+searchResults.talentName+"</h3>");
									
					jQuery.each(searchResults.results, function (user, info ){						
						jQuery("#search-results-list").append(
						'<li><a id="memberId-'+info.user_id+'" href="#member-page?memberId='+info.user_id+'"><span class="user-name-results" style="font-size:12px;">'+
						info.user_name+' | '+info.user_city+', '+info.user_state+'</a></li>'
						).trigger("create").listview("refresh");								
					});// end foreach result
					}); // end getJSON
		//memberID(UserID);
	
	});
}// end GetRecommendedTalent

}// end SearchFunctions