document.addEventListener('deviceready', SimpleUpdate, false);

function SimpleUpdate(){
	
// Fix for Android focus
jQuery('#post-update-content').on("focusin", function(){
		console.log("messageSubject field adjusted for android")
		jQuery("#profile-options").css("height", "1200px");
		jQuery('html, body').animate({
			scrollTop: jQuery('#make-a-post').offset().top - 20		
			});
	});
	jQuery('#post-update-content').on("focusout", function(){
		jQuery("#profile-options").css("height", "100%");
		});

var UserID = window.localStorage.getItem("satUserID");
jQuery("#post-update-button").on("click",function(){
		console.log("Update Button clicked!");
		if(device.platform){
			var platform = device.platform;
		   console.log("platform: "+platform);
		   if(platform == 'iOS'){jQuery("#file-to-upload-container").show();}
		}
	jQuery("#cancel-post-update-container").delay(200).slideDown("fast");
	jQuery("#post-update-button-container").css("display", "none");

	jQuery("#cancel-post-update").on("click",function(){
		jQuery("#make-a-post").slideUp("fast");
		jQuery("#cancel-post-update-container").css("display", "none");
		jQuery("#post-update-button-container").delay(200).slideDown("fast");
		jQuery("#post-update-content").val('');
		});// end cancel
	jQuery("#make-a-post").slideDown("fast");
})// post-update-botton

jQuery("#file-to-upload").on("change",function(event){	
	
	var files = [];
	jQuery.each(event.target.files, function(index, file) {
    var reader = new FileReader();
	
	
    reader.onload = function(event) { 
		
      object = {};
      object.filename = file.name;
      object.data = event.target.result;
      files.push(object);
	  console.log("object.data:")
	  console.log(object.data);
	  window.localStorage.setItem("satImageObject",object.data);
	  
    };  
    reader.readAsDataURL(file);
  });
})// input change
jQuery("#post-submit-button").on("click",function(e){
	event.preventDefault();
	postContent = jQuery("#post-update-content").val();
	if(!postContent){navigator.notification.alert(
		"Wait! You have to put something in the Update Status box!",
		"Share A Talent", 
		"Close")}// end if no post content
	else{
		jQuery.mobile.loading('show');
		var encodedPostContent = encodeURIComponent(postContent);
		cl("postContent: "+encodedPostContent);	
		var hasPhoto = jQuery("#file-to-upload").val();
		if(hasPhoto){  uploadPhotoFunction();} 
		else {
		var updateURL = 'http://www.shareatalent.com/api/updated-activity.php/?userid='+UserID+'&content='+encodedPostContent;
		console.log(updateURL);
		
		jQuery.getJSON(updateURL,function(data){
			console.log(data);
			jQuery.mobile.loading('hide');	
			jQuery("#make-a-post").slideUp("fast");
			jQuery("#cancel-post-update-container").css("display", "none");
			jQuery("#post-update-button-container").delay(200).slideDown("fast");
			jQuery("#post-update-content").val('');
			jQuery('#post-update-messages').slideDown("fast").html('<h3 style="color:red;">Updated</h3>');
			jQuery('#post-update-messages').delay( 2000 ).slideUp( 500 );
			});

		
	}
	}// if no Photo
	});// click

}// SimpleUpdate
function uploadPhotoFunction(){
	console.log("New Upload Process: ")	
var passToken = window.localStorage.getItem("satpass");
var passUserName = window.localStorage.getItem("satUserName");
var tokenURL = 'http://www.shareatalent.com/wp-admin/admin-ajax.php';
var tokenData = {
	action		: "rtmedia_api",
	method		: "wp_login",
	username  	: passUserName,
	password  	: passToken
	};
jQuery.post(tokenURL,tokenData,function(t){
	console.log("Token created");
    tp = JSON.parse(t);
	console.log("Parsed Token:"+tp.data.access_token);
    token = tp.data.access_token;
	window.localStorage.setItem("satToken",token);
	console.log("SAtToken: ");
	console.log(tp.data.access_token)
	console.log("--------------");
	sendFiletoRTMediaV2();
	}).fail(function() {
    console.log("Unable to get token");
  });	
}//uploadPhotoFunction

function sendFiletoRTMediaV2(){	
	var uploadToken = window.localStorage.getItem("satToken");
	
	var imageLocation = jQuery("#file-to-upload").val();
	var imageData = window.localStorage.getItem("satImageObject");
	//var imageData = imageDataPre.replace('data:image/jpeg;base64,','');
	var imageTitle = jQuery("#post-update-content").val();
	console.log(imageTitle);	
	console.log(imageData);	
	var uploadData = {
	action 			: "rtmedia_api",
	method			: "rtmedia_upload_media",
	token 			: uploadToken,
	rtmedia_file 	: imageData,
	title 			: imageTitle,
	context 		: "profile",
    image_type      : "jpeg"
}
console.log("uploadData:");
console.log(uploadData);
console.log("----------------");
	jQuery.post('http://www.shareatalent.com/wp-admin/admin-ajax.php',uploadData,function(u){
		console.log(u);
		JSON.stringify(u);
		var status = u.status;		
		//if(status == 'TRUE'){alert("Got it!")} else {alert("Nope!")}
			jQuery.mobile.loading('hide');
			// from cancel
			jQuery("#make-a-post").slideUp("fast");
			jQuery("#cancel-post-update-container").css("display", "none");
			jQuery("#post-update-button-container").css("display", "block");
			window.localStorage.setItem("SATempImage", null);
			imageData = '';
			var largeImage = document.getElementById('largeImage');
			largeImage.style.display = 'none';
			largeImage.src = imageData;
			smallImage.style.display = 'none';
			smallImage.src = imageData;
			jQuery("#post-update-content").val('');
			jQuery("#post-update-messages").html('<h3 style="color:red;">Posted!</h3>');
			jQuery("#post-update-messages").slideDown("fast");
			jQuery("#post-update-messages").delay(2000).slideUp("fast");
		}),'json'
}// sendFiletoRTMedia
/*
window.onerror = function(msg, url, linenumber) {
    alert('Error message: '+msg+'\nURL: '+url+'\nLine Number: '+linenumber);
    return true;
}
*/