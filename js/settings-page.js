document.addEventListener('deviceready', SettingsFunctions, false);
document.addEventListener('deviceready', updateSettingsButton, false);

function updateSettingsButton() {	
jQuery("#settings-update-button").on("vclick", function(){
	cl("settings button clicked");
	updateSettingsButtonFunctions();
	});
jQuery("#updateSettingsForm").submit(function(event){
	event.preventDefault();
	cl('"Go" button pressed!')
	updateSettingsButtonFunctions();
	});
}

function updateSettingsButtonFunctions() {	
	cl("updateSettingsButton called!")
	jQuery("#settings-page-messages").empty();		
	var UserID = window.localStorage.getItem("satUserID");	
	var zip 	= jQuery("#settings-zip").val();
	var country = encodeURIComponent(jQuery("#settings-country-select option:selected").val());
	var month 	= jQuery("#birth-month-select option:selected").val();
	var year	= jQuery("#birth-year-select option:selected").val();
	var parent 	= jQuery("#parent-select option:selected").val();
	var updateURL = 'http://www.shareatalent.com/api/usersettings/userSettings_update_api/?userid='+UserID+'&zip='+zip+'&country='+country+'&year='+year+'&parent='+parent+'&callback=?';	
				cl(updateURL);
				jQuery.getJSON(updateURL,function(userUpdate){
					jQuery("#settings-page-messages").html('');
					jQuery("#settings-page-messages").slideDown("fast").html(userUpdate.message);
					jQuery("#settings-page-messages").delay(2000).slideUp("fast");
				console.log(userUpdate);
				});// end callback
	
	}// end updateSettingsButton

function SettingsFunctions(){
cl("Settings Functions ready!");
jQuery("a[id^='settings-menu']").on("vclick",function(){
	cl("Settings loading...");
jQuery("#settings-page-messages").empty();
var UserID = window.localStorage.getItem("satUserID");	
	if (UserID>0) 
	{		
		cl('User# '+UserID + " clicked the Settings Page menu");
			var userURL = 'http://www.shareatalent.com/api/usersettings/userSettings_api/?userid='+UserID+'&callback=?';	
				cl(userURL);
				jQuery.getJSON(userURL,
				function(userSettings){					
					cl("User Zip: "+userSettings.zip);				
					// Zip Codes
						jQuery("#settings-zip").empty();
						jQuery("#settings-zip").val(userSettings.zip);
						jQuery("#settings-zip").trigger("change");
					// Country Settings
						jQuery("#settings-country").empty();
						jQuery("#settings-country").val(userSettings.country);
						jQuery("#settings-country").text(userSettings.country);
						jQuery("#settings-country").attr("selected",true);
						jQuery("#settings-country-select").trigger("change");	
					// Birth Month Settings											
						jQuery("#settings-birth-month").empty();
						jQuery("#settings-birth-month").val(userSettings.month);
						jQuery("#settings-birth-month").text(userSettings.month);
						jQuery("#settings-birth-month").attr("selected",true);
						jQuery("#birth-month-select").trigger("change");	
					// Birth Year Settings
						jQuery("#settings-birth-year").empty();
						jQuery("#settings-birth-year").val(userSettings.year);
						jQuery("#settings-birth-year").text(userSettings.year);
						jQuery("#settings-birth-year").attr("selected",true);
						jQuery("#birth-year-select").trigger("change");	
					// Parent Settings
						jQuery("#settings-parent").empty();
						jQuery("#settings-parent").val(userSettings.parent);
						jQuery("#settings-parent").text(userSettings.parent);
						jQuery("#settings-parent").attr("selected",true);
						jQuery("#parent-select").trigger("change");
					jQuery("#settings-update-userid-form").val(UserID);
				});		
		} // end if
	else {
		jQuery("#settings-content").html("<h1>You Need to Login</h1>");
		}
	}); // end function
}// end SettingsFunctions