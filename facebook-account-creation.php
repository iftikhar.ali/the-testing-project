<?php 
$width = $_GET['w'];
$postmessage = $_REQUEST['postmessage'];
$response = parse_signed_request($_REQUEST['signed_request']);
//if(!$response){header("location:".$url); exit;}
function parse_signed_request($signed_request) {
  list($encoded_sig, $payload) = explode('.', $signed_request, 2); 

  $secret = "471e6a79ecd66671956b23e3f5ade422"; // Use your app secret here

  // decode the data
  $sig = base64_url_decode($encoded_sig);
  $data = json_decode(base64_url_decode($payload), true);

  // confirm the signature
  $expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
  if ($sig !== $expected_sig) {
    error_log('Bad Signed JSON signature!');
    return null;
  }

  return $data;
}

function base64_url_decode($input) {
  return base64_decode(strtr($input, '-_', '+/'));
}

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
<div><?php echo $postmessage; ?></div>
<div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '576289135760081',
          status     : true,
          xfbml      : true
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/all/debug.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>

<div id="facebook-signup">
<fb:registration redirect_uri=http://beta.shareatalent.com/api/fb_create_account.php 
fields="[{'name':'name'},{'name':'email'},{'name':'birthday'},{'name':'sat_username','description':'Share a Talent Username','type':'text'},{'name':'password'}]" width="<?php echo $width; ?>" />

</div>
</body>
</html>